const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];


const root = document.querySelector("#root");

const addList = (parentElement, obj) => {

    const list = document.createElement('li');
    const unorderedList = document.createElement('ul');

    for (let key in obj) {
        const listItem = document.createElement('li');
        listItem.innerText = obj[key];
        unorderedList.appendChild(listItem);
    }

    list.appendChild(unorderedList);

    parentElement.appendChild(list);
}

const showInfo = (arr) => {

    const mainList = document.createElement('ul');
    mainList.classList.add("main-list");

    arr.forEach((item,index) => {


        try {

            if (Object.keys(item).length < 3) {
                const author = !!item.author ? '' : 'author';
                const name = !!item.name ? '' : 'name';
                const price = !!item.price ? '' : 'price';
                let prop = `${author} ${name} ${price}`;
                prop = prop.trimStart();

                throw new Error(`Не всі властивості у об'єкта за індексом ${index}. Відсутня властивість: ${prop}`);
            }

            addList(mainList, item);

        } catch (e) {
            console.log(e.message);
        }

    })

    root.appendChild(mainList);
}

showInfo(books);
