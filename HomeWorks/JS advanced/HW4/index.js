//Завдання
// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.
//
// Технічні вимоги:
// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі.
// Список персонажів можна отримати з властивості characters.
// Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані.
// Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

// Необов'язкове завдання підвищеної складності
// Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження.
// Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.
import {createCards} from "./createCards.js";
import {createOneCard} from "./createOneCard.js";


const btn = document.querySelector('.btn');
const blockAnimate = document.querySelector('.animation-block');
const layerBlock = document.querySelector('.layer-block');

const requestURL = 'https://ajax.test-danit.com/api/swapi/films';

const getInfo = (url) => {

   fetch(url)
        .then(response => response.json())
        .then(films => {
            const cards = createCards();

            films.forEach(({name, episodeId, openingCrawl, characters}) => {
                const {card, filmDescription, loaderMin} = createOneCard({name, episodeId, openingCrawl});
                cards.append(card);

                const actorPromises = characters.map(character =>
                    fetch(character)
                        .then(response => response.json())
                        .then(({name}) => {
                            const actor = document.createElement("div");
                            actor.classList.add('actor');
                            actor.textContent = `${name}`;
                            return actor;
                        })
                );

                setTimeout(() => {
                    Promise.all(actorPromises)
                        .then(actors => {
                            const actorsContainer = document.createElement('div');
                            actorsContainer.classList.add('actors');
                            actorsContainer.append(...actors);
                            card.insertBefore(actorsContainer, filmDescription);
                            loaderMin.style.display = 'none';
                        });
                }, 3000);

            })
        })
        .catch(err => alert(err))
}

btn.addEventListener('click', () => {

    btn.style.display = 'none';
    blockAnimate.style.display = 'flex';
    layerBlock.classList.add('active');


    setTimeout(() => {
        getInfo(requestURL);
        document.body.style.overflow = 'auto';
        blockAnimate.style.display  = 'none';
        layerBlock.classList.remove('active');
    }, 3000)

})




