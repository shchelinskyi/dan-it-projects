export const createCards = () => {
    const wrapper = document.querySelector('.wrapper');
    const cards = document.createElement('div');
    cards.classList.add('cards');
    wrapper.append(cards);
    return cards;
}