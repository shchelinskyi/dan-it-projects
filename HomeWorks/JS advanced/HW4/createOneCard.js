export const createOneCard = ({name,episodeId,openingCrawl}) => {
    const card = document.createElement("div");
    card.classList.add("card");

    const logo = document.createElement("img");
    const filmName = document.createElement("h3");
    const filmPart = document.createElement("h4");
    const filmDescription = document.createElement("p");
    const loaderMin = document.createElement('span');
    loaderMin.classList.add('loader-min');


    logo.src = "./img/logo.png";
    logo.classList.add('logo');
    filmName.textContent = `Film: ${name}`;
    filmName.classList.add('head');
    filmPart.textContent = `Episode: ${episodeId}`;
    filmPart.classList.add('part');
    filmDescription.innerHTML = `<span>About:</span> ${openingCrawl}`;
    filmDescription.classList.add('description');

    card.append(logo,filmPart,filmName,loaderMin,filmDescription);
    return {card, filmDescription, loaderMin}
}