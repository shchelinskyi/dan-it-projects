export class Card {

    link = './img/avatar/av';
    titleElem;
    textElem;

    constructor(id, title, body, AuthorName, AuthorEmail, AuthorID) {
        this.id = id;
        this.title = title;
        this.body = body;
        this.name = AuthorName;
        this.email = AuthorEmail;
        this.authorID = AuthorID;
        this.element = this.createCard();
    }

    createJSON() {
        const obj = {
            id: this.id,
            userId: this.authorID,
            title: this.title,
            body: this.body
        }
        return JSON.stringify(obj);
    }

    createCard() {

        const card = document.createElement('div');
        card.classList.add('card');
        card.id = this.id;
        card.style.order = Math.floor(Math.random() * 100);

        const cardContent= document.createElement('div');
        cardContent.classList.add('card-content');

        const avatar = new Image(70, 70);
        avatar.classList.add('card-avatar');
        avatar.src = `${this.link}${this.authorID}.png`;

        const mainBlock = document.createElement('div');
        mainBlock.classList.add('card-main-block');

        const author = document.createElement('p');
        author.classList.add('card-author');
        author.innerHTML = `${this.name} <span>${this.email}</span>`;

        const title = document.createElement('h3');
        title.classList.add('card-title');
        title.textContent = this.title;
        this.titleElem = title;

        const textElem = document.createElement('p');
        textElem.classList.add('card-text');
        textElem.textContent = this.body;
        this.textElem = textElem;

        mainBlock.append(author, title, textElem);

        cardContent.append(avatar, mainBlock);

        const btnDelete = document.createElement('button');
        btnDelete.classList.add('btn-del');
        btnDelete.innerText = 'q';
        btnDelete.id = this.id;
        btnDelete.onclick = () => this.delCard();

        const btnChange = document.createElement('button');
        btnChange.classList.add('btn-change');
        btnChange.innerText = 'e';
        btnChange.id = this.id;
        btnChange.onclick = () => this.changeCard();


        card.append(cardContent, btnDelete, btnChange);

        return card;
    }

    delCard() {

        fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
            method: 'DELETE',
        }).then(response => {
            if (!!response.ok) {
                this.element.remove();
                console.log(response);
            } else {
                throw new Error('Network response was not ok');
            }
        }).catch(error => console.error('Error:', error));
    }

    changeCard() {
        const modalChange = document.querySelector('.modal-change');
        modalChange.classList.add('active');
        document.body.style.overflow = 'hidden';

        const input1 = document.querySelector('.modal-change-title');
        const input2 = document.querySelector('.modal-change-text');
        input1.value= this.titleElem.textContent;
        input2.value = this.textElem.textContent;


        const btnClose = document.querySelector('.modal-change-btn-close');
        btnClose.onclick = () => {
            input1.value = '';
            input2.value = '';
            modalChange.classList.remove('active');
            document.body.style.overflow = 'auto';
        }

        const btnChange = document.querySelector('.modal-change-btn-create');
        btnChange.onclick = () => {

            if(!!input1.value && input2.value) {
                this.titleElem.textContent = input1.value;
                this.textElem.textContent = input2.value;
                this.title = input1.value;
                this.body = input2.value;

                fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
                    method: 'PUT',
                    body: this.createJSON(),
                    headers: {
                        'Content-Type': 'application/json'
                    }})
                    .then(response => {
                        if (!!response.ok) {
                            console.log(response.json());
                        } else {
                            throw new Error('Network response was not ok');
                        }
                    }).catch(error => console.error('Error:', error));


                modalChange.classList.remove('active');
                document.body.style.overflow = 'auto';
                input1.value = '';
                input2.value = '';
            }

        }

    }
}