export const insertMark = (text, pos, len) => {
    return text.slice(0, pos) + '<mark>' + text.slice(pos, pos + len) + '</mark>' + text.slice(pos + len);
}

export const searchText = () => {
    setTimeout(() => {

        const input = document.querySelector('.search');

        input.addEventListener('input', (e) => {

            const val = e.target.value.trim();
            const list = document.querySelectorAll('.card-text');

            const blockNoMatches = document.querySelector('.block-no-matches');
            const blockNoMatchesText = document.querySelector('.no-text');
            blockNoMatchesText.innerHTML = `No results for <span>${val}</span>`;

            if (val !== '') {
                blockNoMatches.classList.remove('show');
                let hasMatches = false;

                list.forEach(element => {

                    if (element.innerText.search(val) === -1) {
                        element.closest('.card').classList.add('hide');
                        element.innerHTML = element.innerText;
                    } else {
                        element.closest('.card').classList.remove('hide');
                            const str = element.innerText;
                            element.innerHTML = insertMark(str, element.innerText.search(val), val.length);
                            hasMatches = true;
                    }

                });

                if(!hasMatches) {
                    blockNoMatches.classList.add('show');
                }
            }

            else {
                blockNoMatches.classList.remove('show');
                e.target.value = '';
                list.forEach(element => {
                    element.innerHTML = element.innerText;
                    element.closest('.card').classList.remove('hide');
                })
            }
        })
    })

}

