import {Card} from "./Card.js";

export const createTweet = () => {

    let inputTitle = document.querySelector('.modal-tweet-title');
    let inputText= document.querySelector('.modal-tweet-text');
    const authorName = 'Leanne Graham';
    const authorEmail = 'Sincere@april.biz';
    const authorID = 1;

    if (!!inputTitle.value && !!inputText.value) {
        const cards = document.querySelector('.cards');
        const modal = document.querySelector('.modal-tweet');
        const card = new Card(1, inputTitle.value, inputText.value, authorName, authorEmail, authorID);
        inputTitle.value = '';
        inputText.value = '';
        modal.classList.remove('active');
        card.element.style.order = 0;

        fetch(`https://ajax.test-danit.com/api/json/posts/`, {
            method: 'POST',
            body: card.createJSON(),
            headers: {
                'Content-Type': 'application/json'
            }}).then(response => {
            if (!!response.ok) {
                console.log(response.json());
                cards.prepend(card.element);
            } else {
                throw new Error('Network response was not ok');
            }
        }).catch(error => console.error('Error:', error));
    }

}