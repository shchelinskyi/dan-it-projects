//Створити сторінку, яка імітує стрічку новин соціальної мережі Twitter.
// Технічні вимоги:
// При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій.
// Для цього потрібно надіслати GET запит на наступні дві адреси:
// https://ajax.test-danit.com/api/json/users
// https://ajax.test-danit.com/api/json/posts
// Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
// Кожна публікація має бути відображена у вигляді картки , та включати заголовок, текст,
// а також ім'я, прізвище та імейл користувача, який її розмістив.
// На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки.
// При натисканні на неї необхідно надіслати DELETE запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}.
// Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
// Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти тут.
// Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені. Це нормально, все так і має працювати.
// Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас Card. При необхідності ви можете додавати також інші класи.

import {Card} from "./Card.js";
import {searchText} from "./searchText.js";
import {createTweet} from "./createTweet.js";

const urlUsers = 'https://ajax.test-danit.com/api/json/users';
const urlPosts = 'https://ajax.test-danit.com/api/json/posts';

const loader = document.querySelector('.loader');

//CREATE TWEET
const btnTweet = document.querySelector('.btn-tweet');
const modal = document.querySelector('.modal-tweet');
const modalbtnClose = document.querySelector('.modal-tweet-btn-close');
const modalbtnCreate = document.querySelector('.modal-tweet-btn-create');

btnTweet.addEventListener('click', ()=> {
    modal.classList.add('active');
})

modalbtnClose.addEventListener('click', () => {
    modal.classList.remove('active');
})

//GET DATA
const getData = (urlUsers, urlPosts) => {

    const requestUsers = fetch(urlUsers)
        .then(response => response.json())
        .catch(err => alert(err));

    const requestPost = fetch(urlPosts)
        .then(response => response.json())
        .catch(err => alert(err));

    Promise.all([requestUsers, requestPost]).then(([users, posts]) => {

        posts.forEach(({id, userId, title, body}) => {
            const cards = document.querySelector('.cards');
            const author = users.find(user => user.id === userId);
            const card = new Card(id, title, body, author.name, author.email, author.id);
            cards.append(card.element);
        })

    })

}

const start = ()=> {
    loader.style.display = 'block';

    setTimeout(()=> {
        loader.style.display  = 'none';
        getData(urlUsers, urlPosts);
    }, 3000)
}

start();

searchText();

modalbtnCreate.addEventListener('click', createTweet)



