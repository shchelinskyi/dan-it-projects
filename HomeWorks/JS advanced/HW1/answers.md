**1.Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript**


Об'єкти можуть містити прототипи інших об'єктів. Тобто функціонал одного об'єкта може бути розширено за допомогою функціоналу інших об'єктів. Відповідно, якщо на певному об'єкті ми викликаємо метод, і він безпосередньо відсутній, далі йде пошук у прототипі.



**2.Для чого потрібно викликати super() у конструкторі класу-нащадка?**

Виклик конструкції super() дозволяє викликати конструктор батька для доступу к батьківським властивостям і методам.
