class Employee {

    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        this._age= value;
    }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        this._salary = value;
    }
}

class Programmer extends Employee {

    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this.lang = lang
    }

    get salary() {
        return this._salary * 3;
    }

}

const programmer1 = new Programmer('Alex', 27, 1000, 'English');
const programmer2 = new Programmer('Hulio', 25, 1500, 'Spanish');
const programmer3 = new Programmer('Patrick', 32, 5000, 'French');


console.log(programmer1);
console.log(programmer2);
console.log(programmer3);

