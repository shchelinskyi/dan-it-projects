import {createMap} from "./createMap.js";

export const showInfoAboutLocation = ({continent, country, regionName, city, district, lat, lon}, {ip} ) => {

    console.log(ip);
    const content = document.querySelector('.content');

    district = district || 'NOT USED';

    const contentTitle = document.createElement('h3');
    contentTitle.classList.add('content-title');
    contentTitle.textContent = 'The system find your position';


    const ipAddress = document.createElement('h3');
    ipAddress.classList.add('content-address');
    ipAddress.textContent = `your ip: ${ip}`;

    const blockFindMe = document.createElement('div');
    blockFindMe.classList.add('block-find-me');

    const findMeText = document.createElement('div');
    findMeText.classList.add('find-me-text');

    const textMain = document.createElement('div');
    textMain.classList.add('text-main');
    const textDescription = document.createElement('div');
    textDescription.classList.add('text-description');

    const continentElement = document.createElement('p');
    continentElement.innerHTML = `<span>a</span>Continent: <span>${continent}</span>`;
    const countryElement = document.createElement('p');
    countryElement.innerHTML = `<span>b</span>Country: <span>${country}</span>`;
    const regionNameElement = document.createElement('p');
    regionNameElement.innerHTML = `<span>c</span>Region: <span>${regionName}</span>`;
    const cityElement = document.createElement('p');
    cityElement.innerHTML = `<span>d</span>City: <span>${city}</span>`;
    const districtElement = document.createElement('p');
    districtElement.innerHTML = `<span>e</span>District: <span>${district}</span>`;
    textMain.append(continentElement, countryElement, regionNameElement, cityElement, districtElement);

    const latitudeElement = document.createElement('p');
    latitudeElement.innerHTML = `<span>f</span>Latitude: <span>${lat}</span>`;
    const longitudeElement = document.createElement('p');
    longitudeElement.innerHTML = `<span>g</span>Longitude: <span>${lon}</span>`;
    const mapDescription = document.createElement('div');
    mapDescription.innerText = 'These values are not precise enough to be used to identify a specific address or for legal purposes.'
    textDescription.append(latitudeElement,longitudeElement,mapDescription);

    findMeText.append(textMain);

    blockFindMe.append(findMeText);
    content.append(contentTitle, ipAddress, blockFindMe);

    if (!!lat && !!lon) {
        content.classList.add('show');
        findMeText.append(textDescription);
        createMap(lat, lon);
    } else {
        content.classList.add('show');
    }
}