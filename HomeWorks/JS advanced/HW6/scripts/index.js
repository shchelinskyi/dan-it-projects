import {showInfoAboutLocation} from "./showInfoAboutLocation.js";


const btn = document.querySelector('.btn');


const request = async () => {
    const firstResponse = await fetch('https://api.ipify.org/?format=json');
    const ipData = await firstResponse.json();


    const secondResponse = await fetch(`http://ip-api.com/json/${ipData.ip}?fields=status,message,continent,country,regionName,city,district,lat,lon,query`);
    const data = await secondResponse.json();
    showInfoAboutLocation(data, ipData);
}

btn.addEventListener('click', () => {
    btn.style.display = "none";
    request();
});

