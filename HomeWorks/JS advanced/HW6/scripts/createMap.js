export const createMap = (lt, ln) => {

    //CREATE MAP WITH OPTIONS
    const blockFind = document.querySelector('.block-find-me');
    const mapWrapper = document.createElement('div');
    mapWrapper.classList.add('find-map');
    const mapContent = document.createElement('div');
    mapContent.id = 'map';
    blockFind.append(mapWrapper);
    mapWrapper.append(mapContent);

    const map = L.map('map').setView([lt, ln], 6);

    //BASE MAP
    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);


    //LAYERS
    const basemaps = {
        StreetView: L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',   {attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'}),
        Places: L.tileLayer.wms('http://ows.mundialis.de/services/service?', {layers: 'OSM-Overlay-WMS'}),
        CyclOSM : L.tileLayer('https://{s}.tile-cyclosm.openstreetmap.fr/cyclosm/{z}/{x}/{y}.png', {
            maxZoom: 20,
            attribution: '&copy; <a href="https://github.com/cyclosm/cyclosm-cartocss-style/releases" title="CyclOSM - Open Bicycle render">CyclOSM</a> | Map data: &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }),
        Stamen_Toner : L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/toner/{z}/{x}/{y}{r}.{ext}', {
            attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
            subdomains: 'abcd',
            minZoom: 0,
            maxZoom: 20,
            ext: 'png'
        }),
        Topography: L.tileLayer.wms('http://ows.mundialis.de/services/service?',   {layers: 'TOPO-WMS'}),
    };

    L.control.layers(basemaps).addTo(map);

    //DEFAULT LAYER
    basemaps.StreetView.addTo(map);

    //ADD ICON MARKER
    const icon = L.icon({
        iconUrl: './img/icon.svg',
        iconSize: [40, 40],
    });

    //MARKER KOORD.
   const marker1 = L.marker([lt, ln], {icon: icon})
        .bindPopup('Your are there')
        .addTo(map);
}