
const rootElement = document.documentElement;
const btnChange = document.querySelector('.btn-change');


//вариант 1 через переназначения ключа


window.addEventListener('load', () => {
    if (localStorage.getItem('dark-theme') === 'active') {
    rootElement.setAttribute('dark-theme', 'active')
    }
})

btnChange.addEventListener('click', () => {
    if (!!rootElement.hasAttribute('dark-theme')) {
        rootElement.removeAttribute('dark-theme');
        localStorage.setItem('dark-theme', 'noActive')
    } else {
        rootElement.setAttribute('dark-theme', 'active')
        localStorage.setItem('dark-theme', 'active');
    }
})


//вариант 2 через удаление ключа


// window.addEventListener('load', () => {
//     if (!!localStorage.getItem('dark-theme')) {
//         rootElement.setAttribute('dark-theme', 'active')
//     }
// })
//
// btnChange.addEventListener('click', () => {
//     if (rootElement.hasAttribute('dark-theme')) {
//         rootElement.removeAttribute('dark-theme');
//         localStorage.removeItem('dark-theme')
//     } else {
//         rootElement.setAttribute('dark-theme', 'active')
//         localStorage.setItem('dark-theme', 'active');
//     }
// })