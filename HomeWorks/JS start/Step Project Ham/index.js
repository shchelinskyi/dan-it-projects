// TABS SECTION SERVICES

const tabsList = document.querySelector('.tabs');
const tabsListElements = tabsList.querySelectorAll('li');

const tabsContent = document.querySelector('.tabs-content');
const tabsContentElements = tabsContent.querySelectorAll('li');

for (let i = 0; i < tabsListElements.length; i++) {
    const dataValue = tabsListElements[i].innerText;
    tabsListElements[i].dataset.tab = dataValue;
    tabsContentElements[i].dataset.tab = dataValue;
}

tabsList.addEventListener('click', event => {

    if (event.target !== tabsList) {

        tabsListElements.forEach(item => item.classList.remove('active-tabs'));
        event.target.classList.add('active-tabs');

        tabsContentElements.forEach(element => {
            element.classList.remove('show-item');

            if (event.target.dataset.tab === element.dataset.tab) {
                element.classList.add('show-item');
            }
        })
    }
})

//TABS SECTION AMAZING WORK

const tabsMenu = document.querySelector('.tabs-filter');
const tabsMenuElements = tabsMenu.querySelectorAll('li');

const tabsPictures = document.querySelector('.tabs-pictures');
const tabsPicturesElements = document.getElementsByClassName('tabs-img');

tabsMenu.addEventListener('click', event => {

        if (event.target !== tabsMenu) {

            tabsMenuElements.forEach(item => item.classList.remove('active-tabs-work'));
            event.target.classList.add('active-tabs-work');


            for (let i = 0; i < tabsPicturesElements.length; i++) {
                tabsPicturesElements[i].classList.remove('show-img');

                if (event.target.dataset.tab === tabsPicturesElements[i].dataset.tab) {
                    tabsPicturesElements[i].classList.add('show-img');
                } else if (event.target.dataset.tab === 'All') {
                    tabsPicturesElements[i].classList.add('show-img');
                }
            }
        }
    }
)

//LOAD MORE AMAZING WORK

const btnLoad = document.querySelector('.btn-load-more');

function addPictures(from, end) {

    fetch('./data/links.json')
        .then(response => response.json())
        .then(json => {

            tabsMenuElements.forEach(item => {

                if (!!item.classList.contains('active-tabs-work')) {

                    for (let i = from; i < end; i++) {

                        const tab = json[i].tab;
                        const src = json[i].src;

                        const element = document.createElement('li');
                        element.classList.add('tabs-img');
                        element.dataset.tab = tab;

                        const img = document.createElement('img');
                        img.setAttribute('src', `${src}`);
                        img.setAttribute('alt', `${tab}`);

                        const hoverEl = document.createElement('div');
                        hoverEl.classList.add('hover-block');
                        const p = document.createElement('p');
                        p.innerText = `${tab}`;
                        hoverEl.appendChild(p);

                        element.appendChild(img);
                        element.appendChild(hoverEl);
                        tabsPictures.appendChild(element);

                        for (let i = 0; i < tabsPicturesElements.length; i++) {
                            if (item.dataset.tab === 'All') {
                                tabsPicturesElements[i].classList.add('show-img');
                            } else if (item.dataset.tab === tabsPicturesElements[i].dataset.tab )
                                tabsPicturesElements[i].classList.add('show-img');
                        }

                    }
                }
            })
        })
        .catch(error => alert(error.message))

}

const load = document.querySelector('#load');

btnLoad.addEventListener('click', () => {

    const loadID = setTimeout(() => {
        load.classList.add('animate-show');
        setTimeout(() => {
            load.classList.remove('animate-show');

            if (tabsPicturesElements.length < 13) {
                addPictures(0, 12);
                clearTimeout(loadID);
            } else {
                addPictures(12, 24);
                clearTimeout(loadID);
                btnLoad.classList.add('btn-hide');
            }
        }, 2000)
    },);
});


//SLIDER SECTION ABOUT

const reviews = document.querySelector('.reviews');
const reviewsCard = reviews.querySelectorAll('.review-card');

let slide = document.querySelector('.slider-line');
let slides = document.querySelectorAll('.slider-item');


const btnPrev = document.querySelector('.btn-previous');
const btnNext = document.querySelector('.btn-next');


let slider = [];


for (let i = 0; i < slides.length; i++) {
    slider[i] = {};
    slider[i].src = slides[i].src;
    slider[i].name = slides[i].getAttribute('data-name');
    slides[i].remove();
}


let step = 0;
let offsetNext = 0;
let classUp = true;

for (let i = 0; i < slider.length; i++) {
    drawNext();
}


//SET UP ELEMENT SECTION ABOUT

function findUP() {
    document.querySelectorAll('.slider-item').forEach(item => {
        reviewsCard.forEach(card => {
            if (card.classList.contains('show-review-card')) {
                if (item.dataset.name === card.dataset.name) {
                    item.classList.add('up-element')
                }
            }
        })
    })

}


//NEXT ELEMENT SECTION ABOUT

function drawNext() {

    let img = document.createElement('img');
    img.src = slider[step].src;
    img.classList.add('slider-item');

    if(!!classUp){
        img.classList.add('up-element');
        classUp = false;
    }

    img.dataset.name = slider[step].name;
    img.style.left = offsetNext * 98 + 'px';


    slide.appendChild(img);

    if (step + 1 == slider.length) {
        step = 0;
    } else {
        step++;
    }


    if (offsetNext === 6) {
        offsetNext = 6;
    } else {
        offsetNext++;
    }

    findUP();
}

function left() {

    btnNext.onclick = null;
    let slides2 = document.querySelectorAll('.slider-item');
    let offsetL = 0;
    for (let i = 0; i < slides2.length; i++) {
        slides2[i].style.left = offsetL * 98 - 98 + 'px';
        offsetL++;
    }

    setTimeout(function () {
        drawNext();
        slides2[0].remove();

        btnNext.onclick = left;
    }, 100);
}

btnNext.addEventListener('click', left);


//PREVIOUS ELEMENT SECTION ABOUT


function drawPrev() {

    slide = document.querySelector('.slider-line');

    let img2 = document.createElement('img');
    img2.src = slide.lastElementChild.getAttribute('src');
    img2.dataset.name = slide.lastElementChild.getAttribute('data-name');
    img2.classList.add('slider-item');
    img2.style.left = -98 + 'px';
    slide.insertAdjacentElement("afterbegin", img2);
    findUP();
}


function right() {
    let slides3 = document.querySelectorAll('.slider-item');

    step--;
    if (step < 0) {
        step = 6;
    }
    let offsetR = 0;

    drawPrev();
    slides3 = document.querySelectorAll('.slider-item');
    slides3[7].remove();

    slides3 = document.querySelectorAll('.slider-item');


    setTimeout(function () {
        for (let i = 0; i < slides3.length; i++) {
            slides3[i].style.left = offsetR * 98 + 'px';
            offsetR++;
        }

    }, 300);

}

btnPrev.addEventListener('click', right);


//SECTION ABOUT CLICK IMG


slide.addEventListener('click', event => {
    if (event.target.nodeName === 'IMG') {
        document.querySelectorAll('.slider-item').forEach(item => {
            item.classList.remove('up-element');
        })
        event.target.classList.add('up-element');

        reviewsCard.forEach(element => {
            element.classList.remove('show-review-card');
        })

        reviewsCard.forEach(element => {
            if (event.target.dataset.name === element.dataset.name) {
                element.classList.add('show-review-card');
            }
        })

    }
})

//SECTION GALLERY

const grid3 = document.querySelector('.grid3');

let msnry3 = new Masonry(grid3, {
    itemSelector: '.grid3-item',
    columnWidth: '.grid3-sizer',
    gutter: 3,
    horizontalOrder: true,
});

imagesLoaded(grid3).on('progress', function () {
    msnry3.layout();
});


const grid2 = document.querySelector('.grid2');

let msnry2 = new Masonry(grid3, {
    itemSelector: '.grid2-item',
    columnWidth: 180,
    horizontalOrder: true,
});

imagesLoaded(grid2).on('progress', function () {
    msnry3.layout();
});


const grid = document.querySelector('.grid');

let msnry = new Masonry(grid, {
    itemSelector: '.grid-item',
    columnWidth: 378,
    gutter: 13,
    transitionDuration: '0.8s',
    horizontalOrder: true,
});

imagesLoaded(grid).on('progress', function () {
    msnry.layout();
});


//SECTION GALLERY LOAD

const btnGallery = document.querySelector('#btn-gallery');

function addImage(from, end) {

    fetch('./data/gallery.json')
        .then(response => response.json())
        .then(json => {


            for (let i = from; i < end; i++) {

                const src = json[i].src;

                const item = document.createElement('div');
                item.classList.add('grid-item');

                if (i % 3 === 0) {
                    item.classList.add('grid-item--width3');
                }


                const img = document.createElement('img');
                img.classList.add('modal-img');
                img.setAttribute('src', `${src}`);


                item.appendChild(img);
                grid.appendChild(item);
                msnry.appended(item);


                imagesLoaded(grid).on('progress', function () {
                    msnry.layout();
                });


            }
            showPic();
        })
        .catch(error => alert(error.message))

}

const load2 = document.querySelector('#load2');


btnGallery.addEventListener('click', () => {

    const timerID = setTimeout(() => {
        load2.classList.add('animate-show');
        setTimeout(() => {
            load2.classList.remove('animate-show');

            if (grid.children.length < 13) {
                addImage(0, 12);
                clearTimeout(timerID);
            } else {
                addImage(12, 24);
                btnGallery.classList.add('btn-hide');
                clearTimeout(timerID);
            }
        }, 2000)
    },);

});


//MODAL

const modal = document.querySelector('.modal');
const modalContent = document.querySelector('.modal-content');

const pic = document.getElementsByClassName('modal-img');


function showPic() {
    for (let i = 0; i < pic.length; i++) {
        pic[i].addEventListener('click', e => {
            const el = e.target.cloneNode();
            el.classList.add('full-size');
            modal.classList.add('open');
            modalContent.append(el);
            document.body.style.overflow = 'hidden';

            modalContent.parentNode.addEventListener('click', e => {

                if (e.target.classList.contains('modal')) {
                    modal.classList.remove('open');
                    modalContent.innerHTML = '';
                    document.body.style.overflow = 'auto';
                }
            })
        })
    }
}

showPic();




