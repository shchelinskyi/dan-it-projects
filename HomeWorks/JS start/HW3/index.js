//Реалізувати програму на Javascript, яка знаходитиме всі числа кратні 5 (діляться на 5 без залишку) у заданому діапазоні.
//Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

    //Технічні вимоги:

//Отримати за допомогою модального вікна браузера число, яке введе користувач.
//Вивести в консолі всі числа, кратні 5, від 0 до введеного користувачем числа. Якщо таких чисел немає - вивести в консоль фразу "Sorry, no numbers"
//Обов'язково необхідно використовувати синтаксис ES6 (ES2015) для створення змінних.


    //Необов'язкове завдання підвищеної складності


//Перевірити, чи введене значення є цілим числом. Якщо ця умова не дотримується, повторювати виведення вікна на екран, доки не буде введено ціле число.
//Отримати два числа, m і n. Вивести в консоль усі прості числа
//(http://uk.math.wikia.com/wiki/%D0%9F%D1%80%D0%BE%D1%81%D1%82%D0%BE%D0%B5_%D1%87%D0%B8%D1%81%D0%BB%D0%BE)
//в діапазоні від m до n (менше із введених чисел буде m, більше буде n). Якщо хоча б одне з чисел не відповідає умовам
//валідації, зазначеним вище, вивести повідомлення про помилку і запитати обидва числа знову.


const someNumber = +prompt('Введіть число', 'завдання 1');

let counter = 0;

for(let i = 0; i < someNumber; i++) {

    if(i % 5 == 0 && i != 0) {
        console.log(i);
        counter++;
    }
}

if(counter == 0) {
    console.log('Sorry, no numbers');
}


//Завдання підвищеної складності


let someValue1 = +prompt('Введіть число m', 'завдання 2');
while(!Number.isInteger(someValue1)) {
    someValue1 = +prompt('Введіть число m', 'завдання 2');
}

let someValue2 = +prompt('Введіть число n більше за m', 'завдання 2');
while(!Number.isInteger(someValue2)) {
    someValue2 = +prompt('Введіть число n більше за m', 'завдання 2');
}

while(someValue1>someValue2) {
    someValue1 = +prompt('Введіть число m', 'завдання 2');
    while(!Number.isInteger(someValue1)) {
        someValue1 = +prompt('Введіть число m', 'завдання 2');
    }
    someValue2 = +prompt('Введіть число n більше за m', 'завдання 2');
    while(!Number.isInteger(someValue2)) {
        someValue2 = +prompt('Введіть число n більше за m', 'завдання 2');
    }
}

console.log('Завдання 2');

nextStep:
for(;someValue1 < someValue2; someValue1++) {

   for(let i = 2; i < someValue1; i++) {
       if (someValue1 % i == 0) continue nextStep;
   }

   if(someValue1 != 1) {
       console.log(someValue1);
   }
}


