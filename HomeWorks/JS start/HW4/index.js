//Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.
//Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

    //Технічні вимоги:

//Отримати за допомогою модального вікна браузера два числа.
//Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /.
//Створити функцію, в яку передати два значення та операцію.
//Вивести у консоль результат виконання функції.

    //Необов'язкове завдання підвищеної складності

//Після введення даних додати перевірку їхньої коректності.
//Якщо користувач не ввів числа, або при вводі вказав не числа,
// - запитати обидва числа знову (при цьому значенням за замовчуванням для кожної зі змінних має бути введена інформація раніше).


function calculateSomeNumbers() {

    let result;

    let number1 = prompt('Введіть число 1');
    while (isNaN(+number1) || number1 === "" || number1 == null) {
        number1 = prompt('Введіть число 1', `${number1}`);
    }
    number1 = Number(number1);


    let number2 = prompt('Введіть число 2');
    while (isNaN(+number2) || number2 === "" || number2 == null) {
        number2 = prompt('Введіть число 2', `${number2}`);
    }
    number2 = Number(number2);



    let operator = prompt('Введіть дію: +, -, *, /');


    if (operator === '+') {
        result = number1 + number2;
    } else if (operator === '-') {
        result = number1 - number2;
    } else if (operator === '*') {
        result = number1 * number2;
    } else if (operator === '/') {
        result = number1 / number2;
    } else {
        return;
    }

    console.log(result);
}

calculateSomeNumbers();



