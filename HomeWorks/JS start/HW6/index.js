'use strict'

// Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем.
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

    //Технічні вимоги:

// Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
// 1. При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
// 2. Створити метод getAge() який повертатиме скільки користувачеві років.
// 3. Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем
// (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
// Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.






function createNewUser() {

    const firstName = prompt("Введіть своє ім'я");
    const lastName = prompt("Введіть своє прізвище");
    const askAge = prompt("Введіть дату свого народження у форматі - dd.mm.yyyy");
    const birthday = new Date(askAge.split('.').reverse().join('/'));
    const dateNow = new Date();


    const newUser = {
        firstName: firstName,
        lastName: lastName,
        dateNow: dateNow,
        birthday: birthday,

        getLogin() {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },

        getAge() {
            return Math.floor((this.dateNow.getTime()-this.birthday.getTime()) / (1000 * 60 * 60 * 24 * 365));
        },

        getPassword() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear();
        }

    }

    console.log(newUser);
    console.log(newUser.getAge());
    console.log(newUser.getPassword());
    return newUser;
}

createNewUser();





