// Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку.
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:


// Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент,
// , до якого буде прикріплений список (по дефолту має бути document.body.

// кожен із елементів масиву вивести на сторінку у вигляді пункту списку;


const someArr = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

const container = document.createElement('div');
document.body.appendChild(container);


function outputInfo(arr, parent = document.body) {
    const list = document.createElement('ul');
    parent.appendChild(list);
    arr.forEach(item => {
        const listItem = document.createElement('li');
        if (Array.isArray(item)) {
            outputInfo(item, listItem)
        } else {
            listItem.innerText = item;
        }
        list.appendChild(listItem);
    });
}

outputInfo(someArr, container);
