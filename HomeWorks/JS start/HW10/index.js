// Реалізувати перемикання вкладок (таби) на чистому Javascript.

// Технічні вимоги:

// У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки.
// При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.

// Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.

// Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися.
// При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.

const tabsList = document.querySelector('.tabs');
const tabsListElements = tabsList.querySelectorAll('li');

const tabsContent = document.querySelector('.tabs-content');
const tabsContentElements = tabsContent.querySelectorAll('li');


for (let i = 0; i < tabsListElements.length; i++) {
    const dataValue = tabsListElements[i].innerText;
    tabsListElements[i].dataset.tab = dataValue;
    tabsContentElements[i].dataset.tab = dataValue;
}


tabsList.addEventListener('click', event => {
        if (event.target !== tabsList) {

            tabsListElements.forEach(item => item.classList.remove('active'));
            event.target.classList.add('active');

            tabsContentElements.forEach(element => {
                element.classList.remove('show-item');

                if (event.target.dataset.tab === element.dataset.tab) {
                    element.classList.add('show-item');
                }
            })
        }
    }
)