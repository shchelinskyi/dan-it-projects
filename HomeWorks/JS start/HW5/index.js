//Реалізувати функцію створення об'єкта "юзер".
//Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

    //Технічні вимоги:

//Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
//При виклику функція повинна запитати ім'я та прізвище.
//Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
//Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера,
// з'єднану з прізвищем, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
//Створити юзера за допомогою функції createNewUser().
//Викликати у цього юзера функцію getLogin(). Вивести у консоль результат виконання функції.



    //Необов'язкове завдання підвищеної складності

//Зробити так, щоб властивості firstName та lastName не можна було змінювати напряму.
//Створити функції-сеттери setFirstName() та setLastName(), які дозволять змінити дані властивості.


function createNewUser() {
    const firstName1 = prompt("Введіть своє ім'я");
    const lastName1 = prompt("Введіть своє прізвище");

    const newUser = {
        _firstName: firstName1,
        _lastName: lastName1,

        setFirstName(value) {
            this._firstName = value;
        },

        get firstName() {
            return this._firstName;
        },

        setLastName(value) {
            this._lastName = value;
        },

       get lastName() {
            return this._lastName;
        },

        getLogin() {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        }
    }

    return newUser;
}

console.log(createNewUser().getLogin());

