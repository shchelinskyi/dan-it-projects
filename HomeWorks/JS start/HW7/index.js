// Реалізувати функцію фільтру масиву за вказаним типом даних.
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

//Технічні вимоги:

// Написати функцію filterBy(), яка прийматиме 2 аргументи.
// Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
// Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент,
// за винятком тих, тип яких був переданий другим аргументом.
// Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив [23, null].


const filterBy = (arr, typeData) => arr.filter(element => typeof element !== typeData);

const someArr = ['hello', 'world', 23, '23', null, undefined, 'sss', false];

//check

console.log(filterBy(someArr, 'number'), 'number');
console.log(filterBy(someArr, 'string'), 'string');
console.log(filterBy(someArr, 'boolean'), 'boolean');
console.log(filterBy(someArr, 'object'), 'object');
console.log(filterBy(someArr, 'undefined'), 'undefined');


