// Написати реалізацію кнопки "Показати пароль". Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:

// У файлі index.html лежить розмітка двох полів вводу пароля.
// Після натискання на іконку поруч із конкретним полем - повинні відображатися символи, які ввів користувач, іконка змінює свій зовнішній вигляд.
// У коментарях під іконкою - інша іконка, саме вона повинна відображатися замість поточної.
// Коли пароля не видно - іконка поля має виглядати як та, що в першому полі (Ввести пароль)
// Коли натиснута іконка, вона має виглядати, як та, що у другому полі (Ввести пароль)
// Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях
// Якщо значення збігаються – вивести модальне вікно (можна alert) з текстом – You are welcome;
// Якщо значення не збігаються - вивести під другим полем текст червоного кольору Потрібно ввести однакові значення
// Після натискання на кнопку сторінка не повинна перезавантажуватись
// Можна міняти розмітку, додавати атрибути, теги, id, класи тощо.

const form = document.querySelector('form');
const icons = document.querySelectorAll('.fa-eye');
const input1 = document.querySelector('#input-1');
const input2 = document.querySelector('#input-2');
const error = document.querySelector('.error');

const btn = document.querySelector('.btn');


icons.forEach(icon => {

    icon.addEventListener('click', () => {

        if (icon.classList.contains('fa-eye')) {
            icons.forEach(icon => {
                icon.classList.remove('fa-eye');
                icon.classList.add('fa-eye-slash');
                icon.previousElementSibling.setAttribute('type', 'text');
                btn.setAttribute('disabled', 'true');
            })

        } else {
            icons.forEach(icon => {
                icon.classList.remove('fa-eye-slash');
                icon.classList.add('fa-eye');
                icon.previousElementSibling.setAttribute('type', 'password');
                btn.removeAttribute('disabled');
            })
        }
    })
})


form.addEventListener('input', (e) => {
    if (e.target.nodeName === 'INPUT') {
        error.classList.remove('show-error');
    }
})



form.addEventListener('submit', (e) => {
    e.preventDefault();
        if (input1.value === input2.value) {
            alert('You are welcome');
        } else {
            error.classList.add('show-error');
        }
})
