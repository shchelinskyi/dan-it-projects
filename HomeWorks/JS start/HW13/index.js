//Реалізувати програму, яка циклічно показує різні картинки. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

//Технічні вимоги:

//У папці banners лежить HTML код та папка з картинками.
// При запуску програми на екрані має відображатись перша картинка.
// Через 3 секунди замість неї має бути показано друга картинка.
// Ще через 3 секунди – третя.
// Ще через 3 секунди – четверта.
// Після того, як будуть показані всі картинки - цей цикл має розпочатися наново.
// Після запуску програми десь на екрані має з'явитись кнопка з написом Припинити.
// Після натискання на кнопку Припинити цикл завершується, на екрані залишається показаною та картинка, яка була там при натисканні кнопки.
// Поруч із кнопкою Припинити має бути кнопка Відновити показ, при натисканні якої цикл триває з тієї картинки, яка в даний момент показана на екрані.
// Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.


// Необов'язкове завдання підвищеної складності
// При запуску програми на екрані повинен бути таймер з секундами та мілісекундами, що показує, скільки залишилося до показу наступної картинки.
// Робити приховування картинки та показ нової картинки поступовим (анімація fadeOut/fadeIn) протягом 0.5 секунди.



//COLLECTION
const imgAll = document.querySelectorAll('img');

//BUTTONS
const btnStart = document.querySelector('#btn-start');
const btnStop = document.querySelector('#btn-stop');
const btnActive = document.querySelector('#btn-active');

//SCOREBOARD
const timer = document.querySelector('.timer');

//VARIABLES
let elementIndex = 0;
let secondValue = 2;
let millisecondValue = 99;
let opacity = 0;

//TIMER ID
let clockID;
let fadeInId;
let fadeOutId;

//FUNCTIONS

function buttonsShow() {
    setTimeout(() => {
        btnStop.removeAttribute('hidden');
        btnActive.removeAttribute('hidden');
    }, 1000);
}

function clock() {

    clearTimeout(clockID);

    clockID = setTimeout(clock, 10);
    millisecondValue--;
    timer.innerText = `0${secondValue} : ${millisecondValue}`;

    if (millisecondValue < 10) {
        timer.innerText = `0${secondValue} : 0${millisecondValue}`;
    }

    if (millisecondValue < 0) {
        millisecondValue = 99;
        secondValue--;
    }

    if (secondValue < 0) {
        showNextImg();
        secondValue = 2;
    }

}

function showNextImg() {

    imgAll.forEach(item => {
        item.style.opacity = "0";
        item.classList.remove('active');
    });

    elementIndex++;

    if (elementIndex > 3) {
        elementIndex = 0;
        imgAll[elementIndex].style.opacity = "0";
        imgAll[elementIndex].classList.add('active');
    } else {
        imgAll[elementIndex].style.opacity = "0";
        imgAll[elementIndex].classList.add('active');
    }

    fadeInOut();

}


function fadeInOut() {

    fadeInId = setInterval(changeOpacity, 190);
}


function changeOpacity() {

    if (opacity < 1) {
        opacity += 0.1;
        imgAll[elementIndex].style.opacity = opacity;
    } else {
        clearInterval(fadeInId);
        fadeOutId = setInterval(hide, 190);
    }
}

function hide() {

    if (opacity > 0) {
        opacity -= 0.1;
        imgAll[elementIndex].style.opacity = opacity;
    } else {
        opacity = 0;
        imgAll[elementIndex].style.opacity = opacity;
        clearInterval(fadeOutId);
    }
}


btnStart.addEventListener('click', () => {
    buttonsShow();
    fadeInOut();
    clock();
}, {once:true})


btnStop.addEventListener('click', (e) => {
    btnStop.setAttribute('disabled', 'true');
    btnActive.removeAttribute('disabled');
    btnActive.style.backgroundColor = 'darkblue';
    btnStop.style.backgroundColor = 'brown';
    clearTimeout(clockID);
    clearInterval(fadeInId);
    clearInterval(fadeOutId);

});

btnActive.addEventListener('click', (e) => {
    btnActive.setAttribute('disabled', 'true');
    btnStop.removeAttribute('disabled');
    btnActive.style.backgroundColor = 'brown';
    btnStop.style.backgroundColor = 'darkblue';
    fadeInOut();
    clock();
});


