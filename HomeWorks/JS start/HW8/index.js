// 1. Знайти всі параграфи на сторінці та встановити колір фону #ff0000
// 2. Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль.
//    Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
// 3. Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph
// 4. Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
// 5. Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.


//Task1

const paragraphs = document.querySelectorAll('p');

paragraphs.forEach(item => item.style.backgroundColor = '#ff0000');

//Task2

const optionsList = document.querySelector('#optionsList');
console.log(optionsList);

const parentOptionsList = optionsList.parentElement;
console.log(parentOptionsList);

const childOptionsList = optionsList.childNodes;

console.log('Our nodes - start');

childOptionsList.forEach(item => console.log(`Название: ${item.nodeName}`, `Тип: ${item.nodeType}`));

console.log('Our nodes - end');

//Task3

const testParagraph = document.querySelector('#testParagraph');

testParagraph.textContent = 'This is a paragraph';

//Task4

const mainHeader = document.querySelector('.main-header');
const elementsMainHeader = mainHeader.children;

//console.log(elementsMainHeader);
//Якщо треба вивести одним рядком, якщо окремо, то далі item


elementsMainHeader.forEach(item => {
    console.log(item);
    item.classList.add('nav-item');
})


//Task5

const elementsSectionTitle = document.querySelectorAll('.section-title');

elementsSectionTitle.forEach(item => item.classList.remove('section-title'));


