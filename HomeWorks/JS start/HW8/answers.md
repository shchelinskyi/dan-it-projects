**1. Опишіть своїми словами що таке Document Object Model (DOM)**
   
Це об'єктна модель, яка забезпечує структурування документу. Вона зв'язує веб-сторінку з мовою програмування.
    
**2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?**

InnerHTML дозволяє розпізнавати розмітку, тобто може вивести передане значення тегу і тексту. InnerText назви тегів буде сприймати, як звичайних текст.

**3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?**

Можна звернутися через:

1. document.querySelector(css_selector) aбо document.querySelectorAll(css_selector)
2. document.getElementByID(id), document.getElementByName(name), document.getElementByTagName(tag), document.getElementByClassName(css_class) або ж якщо до декількох, то замість Element вказуємо Elements.

Перший спосіб більш універсальних, оскільки використовується до різних селекторів.



