
const links = document.querySelectorAll('.header__menu-link');
const hamMenu = document.querySelector('.ham-menu');
let device = window.innerWidth > 767 ? 'desktop' : 'mobile';
const hamMenuContent = document.querySelector('.ham-menu-content');

function activeTypeMenu () {
    links.forEach( item => {
        if (device === 'mobile') {
            item.style.display = 'none';
            hamMenu.classList.remove("active");
            hamMenuContent.classList.remove("show");
        } else {
            item.style.display = 'inline-block'
        }
    })

    if (device === 'mobile') {
        hamMenu.classList.add('show')
        console.log(device);
    } else if (device === 'desktop') {
        hamMenu.classList.remove('show')
        console.log(device);
    }
}

activeTypeMenu();

window.addEventListener('resize', () => {
    device = window.innerWidth > 767 ? 'desktop' : 'mobile';
    activeTypeMenu();
})

hamMenu.addEventListener('click', () => {
    hamMenu.classList.toggle('active');
    hamMenuContent.classList.toggle("show");
})