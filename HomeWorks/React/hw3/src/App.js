import {useState, useEffect} from "react";
import { Routes, Route } from 'react-router-dom';
import {getProducts} from "./tools";
import Layout from './pages/Layout';
import Home from './pages/Home';
import CartPage from './pages/CartPage';
import SelectedProductsPage from './pages/SelectedProductsPage';
import NoPage from './pages/NoPage';
import Modal from "./Components/Modal";
import st from './App.module.scss';
import Product from "./pages/Product";
import modals from "./utils";

const App = () => {
    const [openedAddProductModal, setOpenedAddProductModal] = useState(false);
    const [openedDelProductModal, setOpenedDelProductModal] = useState(false);
    const [openedDelFavoriteModal, setOpenedDelFavoriteModal] = useState(false);

    const [products, setProducts] = useState([]);
    const [cartItems, setCartItems] = useState([]);
    const [favoriteItems, setFavoriteItems] = useState([]);
    const [sum, setSum] = useState('');

    const calculateSum = (itemsArray) => {
        let sum = 0;
        for (let item of itemsArray) {
            sum += Number(item.price.replace(/\s+/g, ''));
        }
        const newSum = sum.toLocaleString('ru');
        setSum(newSum);
    }

    useEffect(() => {
        const fetchData = async () => {
            const cardsData = await getProducts("./products.json");
            setProducts([...cardsData]);
        }

        fetchData();

        const itemsArray = JSON.parse(window.localStorage.getItem("items")) || [];
        const favoriteArray = JSON.parse(window.localStorage.getItem("favoriteItems")) || [];

        setCartItems([...itemsArray]);
        setFavoriteItems([...favoriteArray]);
        calculateSum(itemsArray);

    }, [] );

    //Modals logic
    const handleOpenModal = (modalType) => {
        document.body.style.overflow = 'hidden';

        if (modalType === "addProductModal") {
            setOpenedAddProductModal(true);
        } else if (modalType === "delProductModal") {
            setOpenedDelProductModal(true);
        } else if (modalType === "delFavoriteModal") {
            setOpenedDelFavoriteModal(true);
        }
    };
    const handleCloseModal = (modal) => {
        document.body.style.overflow = 'auto';

        if (modal === "addProductModal") {
            setOpenedAddProductModal(false);
        } else if (modal === "delProductModal") {
            setOpenedDelProductModal(false);
        } else if (modal === "delFavoriteModal") {
            setOpenedDelFavoriteModal(false);
        }
    };
    const openModalAddCard = (object) => {
        handleOpenModal("addProductModal");
        window.localStorage.setItem("item", JSON.stringify(object));
    };
    const closeModalAddCard = () => {
        window.localStorage.removeItem("item");
        handleCloseModal("addProductModal");
    };
    const openModalDelProduct = (index) => {
        handleOpenModal("delProductModal");
        window.localStorage.setItem("delIndex", index);
    }
    const closeModalDelProduct = () => {
        window.localStorage.removeItem("delIndex");
        handleCloseModal("delProductModal");
    }
    const openModalDelFav = (index) => {
        handleOpenModal("delFavoriteModal");
        window.localStorage.setItem("favIndex", index);
    }
    const closeModalDelFav = () => {
        window.localStorage.removeItem("favIndex");
        handleCloseModal("delFavoriteModal");
    }

    //Logic main buttons
    const addProductToCart = () => {
        const product = JSON.parse(window.localStorage.getItem("item"));
        const productArray = JSON.parse(window.localStorage.getItem("items")) || [];
        productArray.push(product);
        window.localStorage.setItem("items", JSON.stringify(productArray));

        setCartItems([...productArray]);

        calculateSum([...productArray]);

        handleCloseModal("addProductModal");
    };
    const delProductFromCart = () => {
        const productArray = JSON.parse(window.localStorage.getItem("items"));
        const delIndex = window.localStorage.getItem("delIndex");
        productArray.splice(delIndex, 1);
        window.localStorage.setItem("items", JSON.stringify(productArray));

        setCartItems([...productArray]);
        calculateSum([...productArray]);
        handleCloseModal("delProductModal");
    };
    const addProductToFavorites = (object) => {
        const favoriteArray = JSON.parse(window.localStorage.getItem("favoriteItems")) || [];
        favoriteArray.push(object);
        window.localStorage.setItem("favoriteItems", JSON.stringify(favoriteArray));

        setFavoriteItems([...favoriteArray]);
    };
    const delProductFromFavorites = () => {
        const favoriteArray = JSON.parse(window.localStorage.getItem("favoriteItems"));
        const favIndex = window.localStorage.getItem("favIndex");
        favoriteArray.splice(favIndex, 1);
        window.localStorage.setItem("favoriteItems", JSON.stringify(favoriteArray));

        setFavoriteItems([...favoriteArray]);
        handleCloseModal("delFavoriteModal");
    };
    const delFavoriteOnCard = (object) => {
        const favoriteArray = JSON.parse(window.localStorage.getItem("favoriteItems"));
        const indexElement = favoriteArray.findIndex(obj => obj.article === object.article);
        favoriteArray.splice(indexElement, 1);
        window.localStorage.setItem("favoriteItems", JSON.stringify(favoriteArray));

        setFavoriteItems([...favoriteArray]);
    };

    const actionsAddProductModal = <>
        <button className={st.btnModalDel} onClick={addProductToCart}>OK</button>
        <button className={st.btnModalDel} onClick={closeModalAddCard}>Cancel</button>
    </>;

    const actionsDelProductModal = <>
        <button className={st.btnModalData} onClick={delProductFromCart}>OK</button>
        <button className={st.btnModalData} onClick={closeModalDelProduct}>Cancel</button>
    </>;

    const actionsDelFavoriteModal = <>
        <button className={st.btnModalData} onClick={delProductFromFavorites}>OK</button>
        <button className={st.btnModalData} onClick={closeModalDelFav}>Cancel</button>
    </>;


    return (
            <div className={st.container}>
             <Routes>
                <Route path="/" element={<Layout cartItems={cartItems} favoriteItems={favoriteItems}/>}>
                    <Route index element={<Home products={products} onClick={openModalAddCard} addFavorite={addProductToFavorites}
                                                deleteFavorite={delFavoriteOnCard} favoriteItems={favoriteItems} />}/>
                    <Route path="/:id" element={<Product products = {products} onClick={openModalAddCard} addFavorite={addProductToFavorites}
                                                         deleteFavorite={delFavoriteOnCard} favoriteItems={favoriteItems}/>} />
                    <Route path="cart" element={<CartPage items={cartItems} deleteItems={openModalDelProduct} sum={sum}/>}/>
                    <Route path="selected" element={<SelectedProductsPage favoriteItems={favoriteItems}
                                                                          onClick={openModalDelFav} />}/>
                    <Route path="*" element={<NoPage />} />
                </Route>
            </Routes>
                {!!openedAddProductModal &&
                    <Modal header={modals.addProduct.title} closeButton={true}
                           text={modals.addProduct.text} onClose={() => handleCloseModal("addProductModal")}
                           actions={actionsAddProductModal}
                           typeModal={"addProductModal"}/>}
                {!!openedDelProductModal &&
                    <Modal header={modals.delProduct.title} closeButton={true}
                           text={modals.delProduct.text} onClose={() => handleCloseModal("delProductModal")}
                           actions={actionsDelProductModal}
                           typeModal={"delModal"}/>}

                {!!openedDelFavoriteModal &&
                    <Modal header={modals.delFavorite.title} closeButton={true}
                           text={modals.delFavorite.text} onClose={() => handleCloseModal("delFavoriteModal")}
                           actions={actionsDelFavoriteModal}
                           typeModal={"delModal"}/>}
            </div>
    );
}

export default App;
