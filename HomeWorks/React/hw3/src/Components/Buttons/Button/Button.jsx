import PropTypes from 'prop-types';
import st from './Button.module.scss';

const Button = (props) => {
    const {backgroundColor, text, onClick} = props;
    return (
        <button className={st.btn} style={{ backgroundColor: backgroundColor }} onClick={onClick}>
            {text}
        </button>
    );
}

Button.propTypes = {
    backgroundColor: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func,
}

export default Button;