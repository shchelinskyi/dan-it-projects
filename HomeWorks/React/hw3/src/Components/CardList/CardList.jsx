import PropTypes from 'prop-types';
import Card from "../Card";
import st from './CardList.module.scss';

const CardList = ({products, onClick, addFavorite,
                      deleteFavorite, favoriteItems}) => {
        return (
            <div>
                <h1 className={st.title}>PREMIUM WATCHES</h1>
                <img className={st.banner} src="./img/brand4.png" alt="brand"/>
                <div className={st.cardListContainer}>
                    {products.map((product) =>
                        (<Card key={product.id} product = {product}
                               onClick={onClick}  addFavorite={addFavorite}
                               deleteFavorite={deleteFavorite} favoriteItems={favoriteItems}
                        />))}
                </div>
            </div>
        );
    };

CardList.propTypes = {
    products:PropTypes.array,
    onClick:PropTypes.func.isRequired,
    addFavorite: PropTypes.func.isRequired,
    deleteFavorite: PropTypes.func.isRequired,
    favoriteItems: PropTypes.array,
}
export default CardList;