import PropTypes from 'prop-types';
import st from "./SelectedProductsContainer.module.scss";


const SelectedProductsContainer = ({onClick,favoriteItems}) => {
        return (
            <div>
                <div className={st.selectedWrapper} onClick={onClick}>
                    <img className={st.selectedImg} src="./img/star.png" alt="selected"/>
                    <p className={st.selectedProducts}>({favoriteItems.length})</p>
                </div>
            </div>
        );
    };

SelectedProductsContainer.propTypes = {
    favoriteItems: PropTypes.array,
    onClick: PropTypes.func
}
export default SelectedProductsContainer;