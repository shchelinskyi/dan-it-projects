import PropTypes from 'prop-types';
import st from './CartContainer.module.scss'

const CartContainer = ({cartItems}) => {
        return (
            <div className={st.cart}>
                <img className={st.cartImg} src="./img/cart2.png" alt="cart"/>
                <p className={st.cartProducts}>Shopping cart</p>
                <p className={st.productNumber}>{cartItems.length}</p>
            </div>
        );
    };

CartContainer.propTypes = {
    cartItems: PropTypes.array
}
export default CartContainer;