import PropTypes from 'prop-types';
import {useState, useEffect} from "react";
import Button from "../Buttons/Button";
import st from './Card.module.scss';
import {Link} from "react-router-dom";

const Card = ({product, onClick, favoriteItems, addFavorite, deleteFavorite}) => {

    const [isFavorite, setFavorite] = useState(false);

    useEffect(() => {
        setFavorite(false);
        if (favoriteItems) {
            favoriteItems.forEach((item) => {
                if (item.article === product.article) {
                    setFavorite( true);
                }
            })
        }
    }, [favoriteItems]);
    const toggleFavorite = (object) => {

        if (!isFavorite) {
           addFavorite(object);
        } else {
           deleteFavorite(object);
        }

        setFavorite(!isFavorite);
    };

    const {id, url, name, price} = product;

    return (
        <div className={st.cardWrapper}>
            <img className={st.cardImg} src={url} alt="product"/>
            <Link className={st.linkName} to={`/${id}`}>
            <h2 className={st.productName}>{name}</h2>
            </Link>
            <Link className={st.link} to={`/${id}`}>
            <div className={st.productPrice}>{price} $</div>
            </Link>
            <div className={st.btnBlock}>
                <Link className={st.link} to={`/${id}`}>
                    <Button backgroundColor="firebrick" text="View Product"/>
                </Link>
                <Button backgroundColor="teal" text="Add to Cart" onClick={()=>onClick(product)}/>
            </div>


            {!isFavorite && <img className={st.cardSelected} src="./img/star.png" alt="selected"
                                 onClick={()=> toggleFavorite(product)} />}
            {!!isFavorite && <img className={st.cardSelected} src="./img/star4.png" alt="selected"
                                  onClick={()=> toggleFavorite(product)}
            />}
        </div>
    );

}


Card.propTypes = {
    product:PropTypes.object,
    onClick: PropTypes.func.isRequired,
    addFavorite: PropTypes.func.isRequired,
    deleteFavorite: PropTypes.func.isRequired,
    favoriteItems: PropTypes.array,
}
export default Card;