import PropTypes from 'prop-types';
import st from './SelectedProductsPage.module.scss'
import {Link} from "react-router-dom";

const SelectedProductsPage = ({favoriteItems, onClick}) => {

    return (
        <div>
            <div className={st.title}>Selected Products
                <img className={st.selectedImg} src="./img/star6.png" alt="selected"/>
            </div>
            <div className={st.selectedMain}>
                <h2 className={st.selectedTitle}>Selected products:</h2>
                <div className={st.selectedList}>
                    {!!favoriteItems && favoriteItems.map((obj, index) => (
                            <div key={obj.id}
                                 className={st.selectedProduct}>
                                <div className={st.selectedLeftBlock}>
                                    <Link className={st.link} to={`/${obj.id}`}>
                                    <img className={st.cartLogo} src={obj.url} alt=""/>
                                    </Link>
                                    <Link className={st.link} to={`/${obj.id}`}>
                                    <div>Article: {obj.article}</div>
                                    </Link>
                                </div>

                                <div className={st.selectedRightBlock}>
                                    <Link className={st.link} to={`/${obj.id}`}>
                                    <div>{obj.name}</div>
                                    </Link>
                                    <button className={st.delBtn} onClick={() => onClick(index)}>✖</button>
                                </div>
                            </div>
                        )
                    )}
                </div>
            </div>
        </div>
    )
};

SelectedProductsPage.propTypes = {
    onClick:PropTypes.func.isRequired,
    favoriteItems: PropTypes.array.isRequired,
}
export default SelectedProductsPage;
