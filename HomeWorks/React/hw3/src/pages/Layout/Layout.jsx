import {Outlet, Link} from 'react-router-dom';
import st from './Layout.module.scss';
import CartContainer from "../../Components/CartContainer";
import SelectedProductsContainer from "../../Components/SelectedProductsContainer";
import cn from "classnames";
import PropTypes from "prop-types";


const Layout = ({ cartItems, favoriteItems}) => {
    return (
        <>
            <header className={st.header}>
                <div className={st.titleBlock}>
                        <Link to="/">
                            <img className={st.logo} src="./img/watch.jpg" alt="logo"/>
                        </Link>
                    <div className={st.titleText}>
                        <h3 className={st.title}>PREMIUM WATCHES</h3>
                        <p className={st.slogan}>Time is on your side with our watches</p>
                    </div>
                </div>
                <nav>
                    <ul className={st.navList}>
                        <li>
                            <Link className={cn(st.link, st.linkHome)} to="/">
                                <img className={st.iconHome} src="./img/home.png" alt="home"/>
                            </Link>
                        </li>
                        <li>
                            <Link className={st.link} to="/cart">
                                {<CartContainer cartItems={cartItems}/>}
                            </Link>
                        </li>
                        <li>
                            <Link className={st.link} to="/selected">
                                {<SelectedProductsContainer favoriteItems={favoriteItems}/>}
                            </Link>
                        </li>
                    </ul>
                </nav>
            </header>

            <Outlet/>
        </>
    )
}

Layout.propTypes = {
    cartItems: PropTypes.array.isRequired,
    favoriteItems: PropTypes.array.isRequired,
}

export default Layout;