import {useNavigate, useParams} from 'react-router-dom';
import {useEffect, useState} from "react";
import Button from "../../Components/Buttons/Button";
import PropTypes from "prop-types";
import st from './Product.module.scss';
import cn from 'classnames';

function Product({products, onClick, favoriteItems, addFavorite, deleteFavorite}) {

    const navigate = useNavigate();
    const productID = parseInt(useParams().id);
    const product = products.find((item) => item.id === productID);
    const [isFavorite, setFavorite] = useState(false);

    useEffect(() => {
        setFavorite(false);
        if (favoriteItems) {
            favoriteItems.forEach((item) => {
                if (item.article === product?.article) {
                    setFavorite(true);
                }
            })
        }
    }, [favoriteItems]);
    const changePage = (actionType) => {
        if (productID >= 1 && productID <= products.length) {
            if (actionType === "forward") {
                const page = productID + 1;
                navigate("/" + page);
            } else if (actionType === "back") {
                const page = productID - 1;
                navigate("/" + page);
            }
        }
    }
    const toggleFavorite = (object) => {
        if (!isFavorite) {
            addFavorite(object);
        } else {
            deleteFavorite(object);
        }
        setFavorite(!isFavorite);
    }

    return (
        <>
            <h2 className={st.title}>{product.name}</h2>
            <div className={st.mainContent}>
                <button className={st.btnChangePage} onClick={() => changePage("back")}
                        style={{visibility: productID > 1 ? "visible" : "hidden"}}>
                    <img className={st.imgChangePage} src="./img/back.png" alt="back"/>
                </button>
                <div className={st.productContainer}>
                    <div className={st.product}>
                        <div className={st.imgContainer}>
                            <img className={st.productImg} src={product.url} alt="product"/>
                        </div>
                        <div className={st.productPrice}>{product.price} $</div>
                        <div className={st.productArticle}>Article: {product?.article} </div>
                        <Button backgroundColor="firebrick" text="Add to Cart" onClick={() => onClick(product)}/>

                        {!isFavorite && <img className={st.cardSelected} src="./img/star7.png" alt="selected"
                                             onClick={() => toggleFavorite(product)}/>}
                        {!!isFavorite && <img className={st.cardSelected} src="./img/star4.png" alt="selected"
                                              onClick={() => toggleFavorite(product)}/>}
                    </div>
                    <div className={st.about}>
                        <h1 className={st.titleAbout}>Characteristics:</h1>
                        <div className={st.option}>
                            <div className={st.optionTitle}>Assembly factory:</div>
                            <div className={st.optionValue}>
                                <img className={st.countryImg} src={`./img/country/${product?.factory}.png`}
                                     alt="product"/>
                                {product?.factory}
                            </div>
                        </div>
                        <div className={st.option}>
                            <div className={st.optionTitle}>Watch mechanism:</div>
                            <div className={cn(st.optionValue, st.valueMechanism)}>{product.mechanism}</div>
                        </div>
                        <div className={st.option}>
                            <div className={st.optionTitle}>Guarantee:</div>
                            <div className={st.optionValue}>{product.guarantee}</div>
                        </div>
                        <div className={st.option}>
                            <div className={st.optionTitle}>Type:</div>
                            <div className={st.optionValue}>{product.type}</div>
                        </div>
                        <div className={st.option}>
                            <div className={st.optionTitle}>Color:</div>
                            <div className={st.optionValue}>
                                <div className={st.colorElement} style={{backgroundColor: product?.color}}></div>
                                {product.color}
                            </div>
                        </div>

                    </div>
                </div>
                <button className={st.btnChangePage} onClick={() => changePage("forward")}
                        style={{visibility: productID < products.length ? "visible" : "hidden"}}>
                    <img className={st.imgChangePage} src="./img/next.png" alt="next"/>
                </button>
            </div>


        </>
    );
}


Product.propTypes = {
    products: PropTypes.array.isRequired,
    onClick: PropTypes.func.isRequired,
    addFavorite: PropTypes.func.isRequired,
    deleteFavorite: PropTypes.func.isRequired,
    favoriteItems: PropTypes.array.isRequired,
}

export default Product;