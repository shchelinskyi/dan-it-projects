import CardList from "../../Components/CardList";
import PropTypes from "prop-types";


const Home = ({products, onClick, addFavorite, deleteFavorite, favoriteItems})=> {
    return(
        <>
            {!!products && <CardList products={products} onClick={onClick} addFavorite={addFavorite}
                                                     deleteFavorite={deleteFavorite} favoriteItems={favoriteItems}/>}
        </>
    )
}


Home.propTypes = {
    products: PropTypes.array.isRequired,
    favoriteItems: PropTypes.array.isRequired,
    onClick: PropTypes.func.isRequired,
    addFavorite: PropTypes.func.isRequired,
    deleteFavorite: PropTypes.func.isRequired,
}
export default Home;




