import PropTypes from 'prop-types';
import st from './CartPage.module.scss'
import {Link} from "react-router-dom";

const CartPage = ({items, deleteItems, sum}) => {

    return (
        <div className={st.cartWrapper}>
            <div className={st.title}>Shopping Cart <img className={st.cartImg} src="./img/cartTitle4.png" alt="cart"/>
            </div>
            <div className={st.cartMain}>
                <h2 className={st.cartTotal}>Total: {sum} $</h2>
                <div className={st.cardList}>
                    {!!items && items.map((obj, index) => (
                            <div key={obj.article} className={st.cartProduct}>
                                <div className={st.cartLeftBlock}>
                                    <Link className={st.link} to={`/${obj.id}`}>
                                    <img className={st.cartLogo} src={obj.url} alt=""/>
                                    </Link>
                                    <Link className={st.link} to={`/${obj.id}`}>
                                        <div>{obj.name}</div>
                                    </Link>
                                </div>
                                <div className={st.cartRightBlock}>
                                    <Link className={st.link} to={`/${obj.id}`}>
                                    <div>{obj.price} $</div>
                                    </Link>
                                    <button className={st.delBtn} onClick={() => deleteItems(index)}>✖</button>
                                </div>
                            </div>
                        )
                    )}
                    {items.length > 0 && <button className={st.checkoutBtn}>Checkout</button>}
                </div>
            </div>
        </div>
    )
};

CartPage.propTypes = {
    items: PropTypes.array.isRequired,
    deleteItems: PropTypes.func.isRequired,
    sum: PropTypes.string.isRequired,
}
export default CartPage;