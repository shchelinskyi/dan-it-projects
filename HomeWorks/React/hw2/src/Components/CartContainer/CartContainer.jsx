import {Component} from 'react';
import PropTypes from 'prop-types';
import st from './CartContainer.module.scss'

class CartContainer extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const{onClick, items} = this.props;
        return (
            <div className={st.cart} onClick={onClick}>
                <img className={st.cartImg} src="./img/cart2.png" alt="cart"/>
                <p className={st.cartProducts}>Shopping cart</p>
                <p className={st.productNumber}>{items.length}</p>
            </div>
        );
    }
}

CartContainer.propTypes = {
    onClick: PropTypes.func.isRequired,
    items: PropTypes.array
}
export default CartContainer;