import {Component} from "react";
import PropTypes from 'prop-types';
import st from './SelectedProducts.module.scss'

class SelectedProducts extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const {favoriteItems} = this.props;

        return (
            <div className={st.selectedWrapper}>
                <h2 className={st.selectedTitle}>Selected products:</h2>
                <div className={st.selectedList}>
                    {!!favoriteItems && favoriteItems.map((obj,index) => (
                            <div key={obj.article*(Math.floor(Math.random() * 1000) + 1)}
                                 className={st.selectedProduct}>
                                <div>Article: {obj.article}</div>
                                <div className={st.selectedRightBlock}>
                                    <div>{obj.name}</div>
                                </div>
                            </div>
                        )
                    )}
                </div>
            </div>
        )
    }
}

SelectedProducts.propTypes = {
    favoriteItems: PropTypes.array,
}
export default SelectedProducts;