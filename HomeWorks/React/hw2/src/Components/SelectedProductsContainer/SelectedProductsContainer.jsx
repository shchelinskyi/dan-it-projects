import {Component} from 'react';
import PropTypes from 'prop-types';
import st from "./SelectedProductsContainer.module.scss";


class SelectedProductsContainer extends Component {
    render() {
        const {onClick,favoriteItems} = this.props;
        return (
            <div>
                <div className={st.selectedWrapper} onClick={onClick}>
                    <img className={st.selectedImg} src="./img/star.png" alt="selected"/>
                    <p className={st.selectedProducts}>({favoriteItems.length})</p>
                </div>
            </div>
        );
    }
}

SelectedProductsContainer.propTypes = {
    favoriteItems: PropTypes.array,
    onClick: PropTypes.func
}
export default SelectedProductsContainer;