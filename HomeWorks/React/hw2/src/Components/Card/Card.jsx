import {Component} from 'react';
import PropTypes from 'prop-types';
import Button from "../Buttons/Button";
import st from './Card.module.scss';

class Card extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isFavorite: false,
        }

    }

    componentDidMount() {
        const favorites = this.props.favoriteItems;

        if (!!favorites) {

            favorites.forEach((item) => {
                if (item.article === this.props.article) {
                    this.setState({isFavorite: true})
                }
            })
        }
    }

    toggleFavorite = (object) => {

        if (!this.state.isFavorite) {
            this.props.addFavorite(object);
        } else {
            this.props.deleteFavorite(object);
        }

        this.setState(prevState => ({
            isFavorite: !prevState.isFavorite
        }))

    }
    render() {
        const {name, article, price, color, url, onClick, favoriteItems} = this.props;
        const {isFavorite} = this.state;
        const obj = {name, article, price, color, url};
        return (
            <div className={st.cardWrapper}>
                <img className={st.cardImg} src={url} alt="product"/>
                    <h2 className={st.productName}>{name}</h2>
                    <div className={st.productArticle}>Article: {article}</div>
                <div className={st.productPrice}>{price} $</div>
                <div className={st.productColor}>Color: {color}</div>
                <Button backgroundColor="teal" text="Add to cart" onClick={()=>onClick(obj)}/>
                {!isFavorite && <img className={st.cardSelected} src="./img/star.png" alt="selected"
                                     onClick={()=> this.toggleFavorite(obj)} />}
                {!!isFavorite && <img className={st.cardSelected} src="./img/star4.png" alt="selected"
                                      onClick={()=> this.toggleFavorite(obj)}
                                       />}
            </div>
        );
    }
}


Card.propTypes = {
    name:PropTypes.string.isRequired,
    article: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    price: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    color: PropTypes.string.isRequired,
    url:PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    addFavorite: PropTypes.func.isRequired,
    deleteFavorite: PropTypes.func.isRequired,
    favoriteItems: PropTypes.array,
}
export default Card;