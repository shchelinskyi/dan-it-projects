import {Component} from "react";
import PropTypes from 'prop-types';
import st from './Cart.module.scss'

class Cart extends Component {


    render() {

        const {items, deleteItems,sum} = this.props;

        return (
            <div className={st.cartWrapper}>
                <h2 className={st.cartTotal}>Total: {sum} $</h2>
                <div className={st.cardList}>
                    {!!items && items.map((obj,index) => (
                        <div key={obj.article*(Math.floor(Math.random() * 1000) + 1)} className={st.cartProduct}>
                            <div>{obj.name}</div>
                            <div className={st.cartRightBlock}>
                                <div>{obj.price} $</div>
                                <button className={st.delBtn} onClick={()=> deleteItems(index)}>✖</button>
                            </div>
                        </div>
                    )
                    )}
                </div>
            </div>
        )
    }
}

Cart.propTypes = {
    items:PropTypes.array,
    deleteItems:PropTypes.func.isRequired,
    sum: PropTypes.number,
}
export default Cart;