import {Component} from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import st from './Modal.module.scss';


class Modal extends Component {


    clickOutside = (e) => {
        if (e.target === e.currentTarget) {
            this.props.onClose();
        }
    }
    render() {
        const {header, closeButton, text, actions, onClose,typeModal} = this.props;

        return (
            <div className={st.modalWrapper} onClick={this.clickOutside}>
                <div className={cn(st.modal,st[typeModal])}>
                    <div className={st[`${typeModal}Header`]}>
                        <h1 className={st[`${typeModal}Title`]}>{header}</h1>
                        {!!closeButton &&
                            <button className={st[`${typeModal}Close`]} onClick={onClose}>✖</button>}
                    </div>
                    <p className={st[`${typeModal}Text`]}>{text}</p>
                    <div className={st[`${typeModal}Actions`]}>{actions}</div>
                </div>
            </div>
        );
    }
}

Modal.propTypes = {
    header: PropTypes.string.isRequired,
    closeButton: PropTypes.bool,
    text: PropTypes.string.isRequired,
    actions: PropTypes.element.isRequired,
    onClose: PropTypes.func.isRequired,
    typeModal:PropTypes.string.isRequired,
}

export default Modal;