import {Component} from 'react';
import PropTypes from 'prop-types';
import st from './Button.module.scss';

class Button extends Component {
    render() {
        const {backgroundColor, text, onClick} = this.props;

        return (
            <button className={st.btn} style={{ backgroundColor: backgroundColor }} onClick={onClick}>
                {text}
            </button>
        );
    }
}

Button.propTypes = {
    backgroundColor: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
}

export default Button;