import {Component} from 'react';
import PropTypes from 'prop-types';
import Card from "../Card";
import st from './CardList.module.scss';

class CardList extends Component {
    render() {
        const {products, onClick, addFavorite, deleteFavorite, favoriteItems} = this.props;
        return (
            <div>
                <h1 className={st.title}>PREMIUM WATCH</h1>
                <img className={st.logo} src="./img/brand4.png" alt="brand"/>
                <div className={st.cardListContainer}>
                    {products.map(({name, price, url, article, color}) =>
                        (<Card key={article} name={name} price={price} url={url}
                               article={article} color={color}
                               onClick={onClick}  addFavorite={addFavorite}
                               deleteFavorite={deleteFavorite} favoriteItems={favoriteItems}
                        />))}
                </div>
            </div>
        );
    }
}

CardList.propTypes = {
    products:PropTypes.array,
    onClick:PropTypes.func.isRequired,
    addFavorite: PropTypes.func.isRequired,
    deleteFavorite: PropTypes.func.isRequired,
    favoriteItems: PropTypes.array,
}
export default CardList;