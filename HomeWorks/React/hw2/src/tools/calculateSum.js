const calculateSum = (itemsArray) => {
    let sum = 0;
    for (let item of itemsArray) {
        sum += Number(item.price.replace(/\s+/g, ''));
    }
    return sum.toLocaleString('ru');
}

export default calculateSum;