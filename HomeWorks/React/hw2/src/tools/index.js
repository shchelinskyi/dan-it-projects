import getProducts from "./getProducts";
import calculateSum from "./calculateSum";

export {getProducts, calculateSum};
