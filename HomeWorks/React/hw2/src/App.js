import {Component} from "react";
import {getProducts, calculateSum} from "./tools";
import Modal from "./Components/Modal";
import CardList from "./Components/CardList";
import Cart from "./Components/Cart";
import CartContainer from "./Components/CartContainer";
import SelectedProductsContainer from "./Components/SelectedProductsContainer";
import SelectedProducts from "./Components/SelectedProducts/SelectedProducts";
import st from './App.module.scss';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openedAddProductModal: false,
            openedSecondModal: false,
            products:[],
            cartItems:[],
            showCards: false,
            favoriteItems:[],
            sum: 0,
            showSelected: false,
        }
    }

    async componentDidMount() {
        const cardsData = await getProducts("./products.json");
        const itemsArray = JSON.parse(window.localStorage.getItem("items")) || [];
        const favoriteArray = JSON.parse(window.localStorage.getItem("favoriteItems")) || [];

        this.setState({
            products:cardsData,
            cartItems: itemsArray,
            favoriteItems: favoriteArray,
            sum: calculateSum(itemsArray)
        });
    }

    handleOpenModal = (modal) => {
        document.body.style.overflow = 'hidden';

        if (modal === "addProductModal") {
            this.setState({
                openedAddProductModal: true,
            })
        } else if (modal === "second") {
            this.setState({
                openedSecondModal: true,
            })
        }

    }

    handleCloseModal = (modal) => {
        document.body.style.overflow = 'auto';

        if (modal === "addProductModal") {
            this.setState({
                openedAddProductModal: false,
            })
        } else if (modal === "second") {
            this.setState({
                openedSecondModal: false,
            })
        }
    }

    //handle btn add to cart
    addCard = (object) => {
        this.handleOpenModal("addProductModal");
        window.localStorage.setItem("item", JSON.stringify(object));
        this.setState({
            showCards: false,
            showSelected: false,
        })
    }

    //logic with product to cart
    addItems = () => {
        const product = JSON.parse(window.localStorage.getItem("item"));
        const productArray = JSON.parse(window.localStorage.getItem("items")) || [];
        productArray.push(product);
        window.localStorage.setItem("items", JSON.stringify(productArray));
        this.setState({
            cartItems: productArray,
            sum: calculateSum(productArray)
        });
        this.handleCloseModal("addProductModal");
    }

    deleteItems = (index) => {
        const productArray = JSON.parse(window.localStorage.getItem("items"));
        productArray.splice(index,1);
        window.localStorage.setItem("items", JSON.stringify(productArray));

        this.setState({
            cartItems: productArray,
            sum: calculateSum(productArray)
        });

    }

    clearItem = () => {
        window.localStorage.removeItem("item");
        this.handleCloseModal("addProductModal");
    }

    //logic with favorites
    addFavorite = (object) => {
        const favoriteArray = JSON.parse(window.localStorage.getItem("favoriteItems")) || [];
        favoriteArray.push(object);
        window.localStorage.setItem("favoriteItems", JSON.stringify(favoriteArray));
        this.setState({
            favoriteItems: favoriteArray,
        })
    }

    deleteFavorite = (object) => {
        const favoriteArray = JSON.parse(window.localStorage.getItem("favoriteItems"));
        const indexElement = favoriteArray.findIndex(obj => obj.article === object.article);
        favoriteArray.splice(indexElement,1);
        window.localStorage.setItem("favoriteItems", JSON.stringify(favoriteArray));
        this.setState({
            favoriteItems: favoriteArray,
        })
    }

    showProducts = () => {
        this.setState(prevState => ({
            showCards: !(prevState.showCards),
            showSelected: false,
        }))
    }

    showSelect = () => {
        this.setState(prevState => ({
            showCards: false,
            showSelected: !(prevState.showSelected),
        }))
    }


    render() {

        const {openedAddProductModal, openedSecondModal, products,
            showCards, cartItems, favoriteItems,sum, showSelected} = this.state;

        const titleAddProductModal = "Add this product to cart?";
        const textAddProductModal = "After adding the product to the cart, you can place an order on our website.";

        const headerSecondModal = "Do you want to save this data?";
        const secondModalText = "Your data won't be saved! Are you sure you want to quit registration process?";

        const actionsAddProductModal = <>
            <button className={st.btnModalDel} onClick={this.addItems}>OK</button>
            <button className={st.btnModalDel} onClick={this.clearItem}>Cancel</button>
        </>;

        const actionsDataModal = <>
            <button className={st.btnModalData} onClick={() => this.handleCloseModal("second")}>Cancel</button>
            <button className={st.btnModalData} onClick={() => this.handleCloseModal("second")}>Yes</button>
        </>;


        return (
            <div className={st.container}>
                <CartContainer onClick={()=> this.showProducts()} items={cartItems}/>
                <SelectedProductsContainer onClick={()=> this.showSelect()} favoriteItems={favoriteItems}/>
                {!!openedAddProductModal &&
                    <Modal header={titleAddProductModal} closeButton={true}
                           text={textAddProductModal} onClose={() => this.handleCloseModal("addProductModal")}
                           actions={actionsAddProductModal}
                           typeModal={"addProductModal"}/>
                }
                {!!products && <CardList products={products} onClick={this.addCard} addFavorite={this.addFavorite}
                                         deleteFavorite={this.deleteFavorite} favoriteItems={favoriteItems}
                /> }
                {!!showCards && <Cart items={cartItems} deleteItems={this.deleteItems} sum={sum}/>}
                {!!showSelected && <SelectedProducts favoriteItems={favoriteItems}/>}

                {!!openedSecondModal &&
                    <Modal header={headerSecondModal} closeButton={true}
                           text={secondModalText} onClose={() => this.handleCloseModal("second")}
                           actions={actionsDataModal}
                           typeModal={"dataModal"}/>
                }
            </div>
        )
    }

}

export default App;
