import {Component} from "react";
import st from './App.module.scss';
import Button from "./Components/Buttons/Button";
import Modal from "./Components/Modal";



class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openedFirstModal: false,
            openedSecondModal: false,
        }
    }

    handleOpenModal = (modal) => {

        if (modal === "first") {
            this.setState({
                openedFirstModal: true,
            })
        } else if (modal === "second") {
            this.setState({
                openedSecondModal: true,
            })
        }

    }

    handleCloseModal = (modal) => {

        if (modal === "first") {
            this.setState({
                openedFirstModal: false,
            })
        } else if (modal === "second") {
            this.setState({
                openedSecondModal: false,
            })
        }
    }


    render() {

        const {openedFirstModal, openedSecondModal} = this.state;

        const headerFirstModal = "Do you want to delete this file?";
        const firstModalText = "Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?";

        const headerSecondModal = "Do you want to save this data?";
        const secondModalText = "Your data won't be saved! Are you sure you want to quit registration process?";

        const actionsDeleteModal = <>
            <button className={st.btnModalDel} onClick={() => this.handleCloseModal("first")}>OK</button>
            <button className={st.btnModalDel} onClick={() => this.handleCloseModal("first")}>Cancel</button>
        </>;

        const actionsDataModal = <>
            <button className={st.btnModalData} onClick={() => this.handleCloseModal("second")}>Cancel</button>
            <button className={st.btnModalData} onClick={() => this.handleCloseModal("second")}>Yes</button>
        </>;


        return (
            <div className={st.container}>
                <Button backgroundColor="red" text="Open first modal"
                        onClick={() => this.handleOpenModal("first")}/>
                <Button backgroundColor="orange" text="Open second modal"
                        onClick={() => this.handleOpenModal("second")}/>
                {!!openedFirstModal &&
                    <Modal header={headerFirstModal} closeButton={true}
                           text={firstModalText} onClose={() => this.handleCloseModal("first")}
                           actions={actionsDeleteModal}
                           typeModal={"deleteModal"}/>
                }

                {!!openedSecondModal &&
                    <Modal header={headerSecondModal} closeButton={true}
                           text={secondModalText} onClose={() => this.handleCloseModal("second")}
                           actions={actionsDataModal}
                           typeModal={"dataModal"}/>
                }
            </div>
        )
    }

}

export default App;
