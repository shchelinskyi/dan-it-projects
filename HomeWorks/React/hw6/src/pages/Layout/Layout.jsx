import {Outlet, Link} from 'react-router-dom';
import CartContainer from "../../components/CartContainer";
import SelectedProductsContainer from "../../components/SelectedProductsContainer";
import cn from "classnames";
import st from './Layout.module.scss';

const Layout = () => {
    return (
        <>
            <header className={st.header}>
                <div className={st.titleBlock}>
                        <Link to="/"  data-testid="home2">
                            <img className={st.logo} src="./imgAll/watch.jpg" alt="logo"/>
                        </Link>
                    <div className={st.titleText}>
                        <h3 className={st.title}>PREMIUM WATCHES</h3>
                        <p className={st.slogan}>Time is on your side with our watches</p>
                    </div>
                </div>
                <nav>
                    <ul className={st.navList}>
                        <li>
                            <Link className={cn(st.link)} to="/" data-testid="home">
                                <img className={st.iconHome} src="./imgAll/home.png" alt="home"/>
                            </Link>
                        </li>
                        <li>
                            <Link data-testid="cartLink" className={st.link} to="/cart">
                                {<CartContainer/>}
                            </Link>
                        </li>
                        <li>
                            <Link data-testid="selectedLink" className={st.link} to="/selected">
                                {<SelectedProductsContainer/>}
                            </Link>
                        </li>
                    </ul>
                </nav>
            </header>

            <Outlet/>
        </>
    )
}

export default Layout;