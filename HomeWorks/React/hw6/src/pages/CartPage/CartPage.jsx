import {useDispatch, useSelector} from "react-redux";
import {Link} from "react-router-dom";
import {Form} from "../../components";
import {
    addProductToCart,
    removeOneProductFromCart,
    openDelProductFromCartModal, openCartForm
} from "../../redux/actions";
import cn from "classnames";
import st from './CartPage.module.scss'


const CartPage = () => {

    const products = useSelector((state) => state.products);
    const cartProducts = useSelector((state) => state.cart.cartProducts);
    const formIsOpened = useSelector(state => state.cart.cartForm);
    const sum = useSelector((state) => state.cart.cartSum);
    const dispatch = useDispatch();

    const openForm = () => {
       dispatch(openCartForm());
    }

    const openDelModal = (obj) => {
        dispatch(openDelProductFromCartModal(obj));
    };

    const increaseQuantity = (obj) => {
        dispatch(addProductToCart(obj));
    }

    const decreaseQuantity = (obj) => {
        dispatch(removeOneProductFromCart(obj));
    }

    return (
        <div className={st.cartWrapper}>
            <div className={st.title}  data-testid="shoppingCart">Shopping Cart <img className={st.cartImg} src="./imgAll/cartTitle4.png" alt="cart"/>
            </div>
            <div className={st.content}>
                {!formIsOpened && <div className={st.cartMain}>
                    <h2 className={st.cartTotal}>Total: {sum} $</h2>
                    <div className={st.cardList}>
                        {!!cartProducts && cartProducts.map((obj) => {
                                const {quantity} = obj;
                                const {id, url, name, price} = products.find(product => product.id === obj.id);
                                return (
                                    <div key={id} className={st.cartProduct}>
                                        <div className={st.cartLeftBlock}>
                                            <Link className={st.link} to={`/product/${id}`}>
                                                <img className={st.cartLogo} src={url} alt=""/>
                                            </Link>
                                            <Link className={st.link} to={`/product/${id}`}>
                                                <div>{name}</div>
                                            </Link>
                                        </div>
                                        <div className={st.cartRightBlock}>
                                            <div className={st.quantityBlock}>
                                                <button className={st.quantityBtn} onClick={() => decreaseQuantity(obj)}
                                                        disabled={quantity === 1}>-
                                                </button>
                                                <div className={st.quantity}>{quantity}</div>
                                                <button className={st.quantityBtn} onClick={() => increaseQuantity(obj)}>+
                                                </button>
                                            </div>
                                            <div className={st.priceBlock}>
                                                <div>{(Number(price.replace(/\s+/g, '')) * quantity).toLocaleString('ru')} $</div>
                                                <button className={st.delBtn} onClick={() => openDelModal(obj)}>✖</button>
                                            </div>
                                        </div>
                                    </div>
                                )

                            }
                        )}
                        {cartProducts.length > 0 && <button className={st.checkoutBtn} onClick={openForm}>Checkout</button>}
                    </div>
                </div>}
                <div>{!!formIsOpened && <Form/>}</div>
                <div>{!!formIsOpened && <div className={cn(st.orderWrapper)}>
                        <h2 className={st.orderTitle}>YOUR ORDER</h2>
                        <div className={cn(st.ordersContainer, cartProducts.length>5 ? st.ordersOverflow : null)}>
                            {!!cartProducts && cartProducts.map((obj) => {
                                    const {quantity} = obj;
                                    const {id, url, name, price} = products.find(product => product.id === obj.id);
                                    return (
                                        <div key={id} className={st.orderedProduct}>
                                            <div className={st.orderedLeftBlock}>
                                                <Link className={st.link} to={`/product/${id}`}>
                                                    <img className={st.orderedProductLogo} src={url} alt=""/>
                                                </Link>
                                                <Link className={st.nameOrderedProduct} to={`/product/${id}`}>
                                                    <div>{name}</div>
                                                </Link>
                                            </div>
                                            <div className={st.orderedRightBlock}>
                                                    <div className={st.orderedProductQuantity}>{quantity}</div>
                                                    <div>{(Number(price.replace(/\s+/g, '')) * quantity).toLocaleString('ru')} $</div>
                                            </div>
                                        </div>
                                    )

                                }
                            )}
                            <h2 className={st.orderedTotal}>Total: {sum} $</h2>
                        </div>
                    </div>}</div>
            </div>
        </div>
    )
};

export default CartPage;