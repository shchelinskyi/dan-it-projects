import st from './CardList.module.scss';
import Card from "../Card";
import {useSelector} from "react-redux";
import {ProductTable, ViewSwitcher} from "../index";
import {useContext} from "react";
import {ProductContext} from "../../context/ProductContextProvider";

const CardList = () => {

    const products = useSelector((state) => state.products);
    const {viewType} = useContext(ProductContext);

    return (
        <div className={st.cardListWrapper}>
            <h1 className={st.title}>PREMIUM WATCHES</h1>
            <img className={st.banner} src="./imgAll/brand4.png" alt="brand"/>
            <ViewSwitcher/>
            {viewType === "cards"
                ?
                (<div className={st.cardListContainer}>
                    {products.map((product) =>
                        (<Card key={product.id} product={({id: product.id, price: product.price})}/>))}
                </div>)
                :
                (<div className={st.productTableContainer}>
                    <ProductTable/>
                </div>)
            }
        </div>
    );
};

export default CardList;