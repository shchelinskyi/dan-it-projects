import {useSelector} from "react-redux";
import st from "./ProductTable.module.scss";
import {ProductTableItem} from "../index";


const ProductTable = () => {

    const products = useSelector((state) => state.products);


    return (
        <div className={st.container}>
            {!!products &&
                products.map(({id, article, price}) => (<ProductTableItem product={({id, price, article})}/>))}
        </div>
    );
};

export default ProductTable;
