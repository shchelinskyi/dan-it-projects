export const ADD_PRODUCT_TO_CART = "ADD_PRODUCT_TO_CART";
export const REMOVE_ONE_PRODUCT_FROM_CART = "REMOVE_ONE_PRODUCT_FROM_CART";
export const REMOVE_PRODUCT_FROM_CART = "REMOVE_PRODUCT_FROM_CART";
export const UPDATE_SUM = "UPDATE_SUM";

export const addProductToCart = (obj) => {
   return {type: ADD_PRODUCT_TO_CART, payload: obj}
};

export const removeOneProductFromCart = (obj) => {
    return {type:REMOVE_ONE_PRODUCT_FROM_CART, payload: obj}
};

export const  removeProductFromCart = (obj) => {
    return {type: REMOVE_PRODUCT_FROM_CART, payload: obj}
};

export const updateSum = (sum) => {
    return {type: UPDATE_SUM, payload: sum}
}
