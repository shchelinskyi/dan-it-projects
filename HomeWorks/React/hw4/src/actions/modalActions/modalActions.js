export const OPEN_ADD_PRODUCT_MODAL = "OPEN_ADD_PRODUCT_MODAL";
export const CLOSE_ALL_MODALS = "CLOSE_ALL_MODALS";
export const OPEN_DEL_PRODUCT_MODAL = "OPEN_DEL_PRODUCT_MODAL";
export const OPEN_DEL_FAVORITE_MODAL = "OPEN_DEL_FAVORITE_MODAL";

export const openAddProductToCartModal = (obj) => {
    document.body.style.overflow = 'hidden';
    return {type: OPEN_ADD_PRODUCT_MODAL, payload: obj}
};

export const openDelProductFromCartModal = (obj) => {
    document.body.style.overflow = 'hidden';
    return {type: OPEN_DEL_PRODUCT_MODAL, payload: obj}
};

export const openDelProductFromFavoritesModal = (obj) => {
    document.body.style.overflow = 'hidden';
    return {type: OPEN_DEL_FAVORITE_MODAL, payload: obj}
};

export const closeAllModals = () => {
    document.body.style.overflow = 'auto';
    return {type: CLOSE_ALL_MODALS}
};