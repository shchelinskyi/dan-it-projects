import {FETCH_DATA_SUCCESS, fetchProducts} from "./productsActions/productsActions";

import {
    OPEN_ADD_PRODUCT_MODAL,
    CLOSE_ALL_MODALS,
    OPEN_DEL_PRODUCT_MODAL,
    OPEN_DEL_FAVORITE_MODAL,
    openAddProductToCartModal,
    openDelProductFromCartModal,
    openDelProductFromFavoritesModal,
    closeAllModals
} from "./modalActions/modalActions";

import {
    ADD_PRODUCT_TO_CART,
    REMOVE_ONE_PRODUCT_FROM_CART,
    REMOVE_PRODUCT_FROM_CART,
    UPDATE_SUM,
    addProductToCart,
    removeOneProductFromCart,
    removeProductFromCart,
    updateSum,
} from "./cartProductsActions/cartProductsActions";


import {
    ADD_PRODUCT_TO_FAVORITES,
    REMOVE_PRODUCT_FROM_FAVORITES,
    addProductToFavorites,
    removeProductFromFavorites
} from "./favoritesProductsActions/favoritesProductsActions";

export {
    fetchProducts,
    FETCH_DATA_SUCCESS,
    OPEN_ADD_PRODUCT_MODAL,
    CLOSE_ALL_MODALS,
    OPEN_DEL_PRODUCT_MODAL,
    OPEN_DEL_FAVORITE_MODAL,
    ADD_PRODUCT_TO_CART,
    REMOVE_ONE_PRODUCT_FROM_CART,
    REMOVE_PRODUCT_FROM_CART,
    UPDATE_SUM,
    ADD_PRODUCT_TO_FAVORITES,
    REMOVE_PRODUCT_FROM_FAVORITES,
    openAddProductToCartModal,
    openDelProductFromCartModal,
    openDelProductFromFavoritesModal,
    closeAllModals,
    addProductToCart,
    removeOneProductFromCart,
    removeProductFromCart,
    updateSum,
    addProductToFavorites,
    removeProductFromFavorites
}