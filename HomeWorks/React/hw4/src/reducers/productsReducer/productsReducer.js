import {
    FETCH_DATA_SUCCESS
} from '../../actions';


export const productsReducer = (state = [], action) => {
    switch (action.type) {
        case FETCH_DATA_SUCCESS:
            return [...state, ...action.payload];
        default:
            return state;
    }
}
