import {
    OPEN_ADD_PRODUCT_MODAL,
    CLOSE_ALL_MODALS,
    OPEN_DEL_PRODUCT_MODAL,
    OPEN_DEL_FAVORITE_MODAL
} from '../../actions';

const defaultState = {
    openedAddProductModal: false,
    openedDelProductModal: false,
    openedDelFavoriteModal: false,
    selectedProduct: {}
};

export const modalReducer = (state = defaultState, action) => {
    switch (action.type) {
        case OPEN_ADD_PRODUCT_MODAL:
            return {
                ...state,
                openedAddProductModal: true,
                openedDelProductModal: false,
                openedDelFavoriteModal: false,
                selectedProduct: action.payload,
            };
        case OPEN_DEL_PRODUCT_MODAL:
            return {
                ...state,
                openedAddProductModal: false,
                openedDelProductModal: true,
                openedDelFavoriteModal: false,
                selectedProduct: action.payload,
            };
        case OPEN_DEL_FAVORITE_MODAL:
            return {
                ...state,
                openedAddProductModal: false,
                openedDelProductModal: false,
                openedDelFavoriteModal: true,
                selectedProduct: action.payload,
            };
        case CLOSE_ALL_MODALS:
            return {
                ...state, openedAddProductModal: false,
                openedDelFavoriteModal: false,
                openedDelProductModal: false,
                selectedProduct: {},
            };
        default:
            return state;

    }
};