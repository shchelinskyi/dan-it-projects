import st from './CardList.module.scss';
import Card from "../Card";
import {useSelector} from "react-redux";

const CardList = () => {

    const products = useSelector((state) => state.products);

        return (
            <div>
                <h1 className={st.title}>PREMIUM WATCHES</h1>
                <img className={st.banner} src="./img/brand4.png" alt="brand"/>
                <div className={st.cardListContainer}>
                    {products.map((product) =>
                        (<Card key={product.id} product = {({id:product.id, price: product.price})}/>))}
                </div>
            </div>
        );
    };

export default CardList;