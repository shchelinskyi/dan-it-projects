import {useSelector} from "react-redux";
import st from "./SelectedProductsContainer.module.scss";


const SelectedProductsContainer = () => {
    const favorites = useSelector(state =>  state.favorites);
        return (
            <div>
                <div className={st.selectedWrapper}>
                    <img className={st.selectedImg} src="./img/star.png" alt="selected"/>
                    <p className={st.selectedProducts}>({favorites.length})</p>
                </div>
            </div>
        );
    };


export default SelectedProductsContainer;