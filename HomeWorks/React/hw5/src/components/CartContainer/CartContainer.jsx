import {useSelector} from "react-redux";
import st from './CartContainer.module.scss';


const CartContainer = () => {
    const cartItems = useSelector((state) => state.cart.cartProducts);
        return (
            <div className={st.cart}>
                <img className={st.cartImg} src="./img/cart2.png" alt="cart"/>
                <p className={st.cartProducts}>Shopping cart</p>
                <p className={st.productNumber}>{cartItems.length}</p>
            </div>
        );
    };

export default CartContainer;