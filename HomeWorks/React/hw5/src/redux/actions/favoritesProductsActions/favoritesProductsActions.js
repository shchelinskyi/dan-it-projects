export const ADD_PRODUCT_TO_FAVORITES = "ADD_PRODUCT_TO_FAVORITES";
export const REMOVE_PRODUCT_FROM_FAVORITES = "REMOVE_PRODUCT_FROM_FAVORITES";

export const addProductToFavorites = (obj) => {
    return {type: ADD_PRODUCT_TO_FAVORITES, payload: obj}
};

export const  removeProductFromFavorites = (obj) => {
    return {type: REMOVE_PRODUCT_FROM_FAVORITES, payload: obj}
};

