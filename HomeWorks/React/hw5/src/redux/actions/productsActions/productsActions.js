export const FETCH_DATA_SUCCESS = "FETCH_DATA_SUCCESS";

export const fetchDataSuccess = (data) => {
   return {type: FETCH_DATA_SUCCESS, payload: data}
};

export const fetchProducts = (url) => async (dispatch) => {
    try {
       const response = await fetch(url);
       const products = await response.json();
       dispatch(fetchDataSuccess(products));
    } catch(error) {
       console.log(error);
    }
}

