export const ADD_PRODUCT_TO_CART = "ADD_PRODUCT_TO_CART";
export const REMOVE_ONE_PRODUCT_FROM_CART = "REMOVE_ONE_PRODUCT_FROM_CART";
export const REMOVE_PRODUCT_FROM_CART = "REMOVE_PRODUCT_FROM_CART";
export const UPDATE_SUM = "UPDATE_SUM";
export const EMPTY_SHOPPING_CART = "EMPTY_SHOPPING_CART";
export const OPEN_CART_FORM = "OPEN_CART_FORM";
export const CLOSE_CART_FORM = "CLOSE_CART_FORM";
export const SET_ORDER_NUMBER = "SET_ORDER_NUMBER";

export const addProductToCart = (obj) => {
   return {type: ADD_PRODUCT_TO_CART, payload: obj}
};

export const removeOneProductFromCart = (obj) => {
    return {type:REMOVE_ONE_PRODUCT_FROM_CART, payload: obj}
};

export const  removeProductFromCart = (obj) => {
    return {type: REMOVE_PRODUCT_FROM_CART, payload: obj}
};

export const updateSum = (sum) => {
    return {type: UPDATE_SUM, payload: sum}
}

export const emptyShoppingCart = () => {
    window.localStorage.removeItem("items");
    window.localStorage.removeItem("firstName");
    window.localStorage.removeItem("lastName");
    window.localStorage.removeItem("age");
    window.localStorage.removeItem("deliveryAddress");
    window.localStorage.removeItem("tel");
    return {type: EMPTY_SHOPPING_CART}
}

export const openCartForm = () => {
    return {type: OPEN_CART_FORM}
}

export const closeCartForm = () => {
    return {type: CLOSE_CART_FORM}
}

export const setOrderNumber = (num) => {
    return {type: SET_ORDER_NUMBER, payload: num}
}
